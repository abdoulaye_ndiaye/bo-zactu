/**
 * Created by ghambyte on 17/06/2017.
 */
$(document)
    .ready(
        function (e) {
            var idtosend = getUrlParameter('id');
            var userPhoto = getUrlParameter('url');
            var username = getUrlParameter('user');
            var idsender = $(".user-informations").attr('id');
            var config = {
                apiKey: "AIzaSyBsfwHz82n_ZM15OZPoWNpoFg73uUsHIHg",
                authDomain: "modelic-7b810.firebaseapp.com",
                databaseURL: "https://modelic-7b810.firebaseio.com",
                projectId: "modelic-7b810",
                storageBucket: "modelic-7b810.appspot.com",
                messagingSenderId: "737888261565"
            };
            firebase.initializeApp(config);

            var topUserPostsRef = firebase.database().ref('messages/' +idtosend +'/'+idsender);
            var topUserPostsRefSender = firebase.database().ref('messages/' +idsender+'/'+idtosend);
//            var topUserInfos = firebase.database().ref('messages/' +idtosend +'/'+idsender +"/"+"/infos");
            var topUserInfos = firebase.database().ref('messages/'+idsender);

            $("#send-message").click(function(){
                var newPostRef = topUserPostsRef.push();
                var newPostRef1 = topUserPostsRefSender.push();
                var date = new Date().getDate();
                var year = new Date().getFullYear();
                var time = new Date().getTime();
                newPostRef.set({
                    "message": $("#message-content").val(),
                    "datemessage": date+"-"+year+"-"+time,
                    "photourlSender": $(".img-user").attr('id'),
                    "usernameSender": $("#user-name-id").text(),
                    "photourlReceiver": ""+userPhoto,
                    "usernameReceiver": ""+username
                });
                newPostRef1.set({
                    "message": $("#message-content").val(),
                    "datemessage": date+"-"+year+"-"+time,
                    "photourlSender": $(".img-user").attr('id'),
                    "usernameSender": $("#user-name-id").text(),
                    "photourlReceiver": ""+userPhoto,
                    "usernameReceiver": ""+username
                });
                $("#message-content").val('');
//               firebase.database().ref('messages/' +idtosend +'/'+idsender +"/"+"/infos").set({
//                   "photourlSender": $(".img-user").attr('id'),
//                   "usernameSender": $("#user-name-id").text()
//               });
//               firebase.database().ref('messages/' +idsender +'/'+idtosend +"/"+"/infos").set({
//                   "photourlSender": $(".img-user").attr('id'),
//                   "usernameSender": $("#user-name-id").text()
//               });
                // console.log("photo "+photoSender);
            });

//            topUserPostsRef.on('child_added', function(snapshot) {
//                var json = JSON.stringify(snapshot.val());
//                var data = JSON.parse(json);
////                console.log(data);
//                var html = '';
//                html += '<li>';
//                html += '<div class="chat-img"><img src="'+data.photourlSender+'" alt="user"></div>';
//                html += '<div class="chat-content">';
//                html += '<h5>'+data.usernameSender+'</h5>';
//                html += '<div class="box bg-light-primary">'+data.message+'</div>';
//                html += '</div>';
//                html += '<div class="chat-time">'+data.datemessage+'</div>';
//                html += '</li>';
//                $("#message-list").append(html);
//            });
            topUserInfos.on('child_added', function(snapshot) {
                $("#chat-sender").html('');
//                 var json = JSON.stringify(snapshot.val());
//                 var data = JSON.parse(json);
//                 console.log(snapshot.key);
                var thisid = snapshot.key;
                snapshot.forEach(function(childSnapshot) {
                 var json = JSON.stringify(childSnapshot.val());
                 var data = JSON.parse(json);
//                 console.log(data);
                    var html = '';
                    html += '<li>';
                    html += '<a id="'+thisid+'" class="chatter" href="javascript:void(0)"><img src="'+data.photourlSender+'" alt="user-img" class="img-circle"> <span> '+data.usernameSender+' <small class="text-success">'+data.message+'</small></span></a>';
                    html += '</li>';
                    $("#chat-sender").html(html);
                });
            });
            $('body').on('click', 'a.chatter', function() {
                // do something
                $("#message-list").html("");
                var iduser = $(this).attr('id');
                console.log(iduser);
                var thisUserRef = firebase.database().ref('messages/'+idsender+'/'+iduser);
                thisUserRef.on('child_added', function(snapshot) {
                    var json = JSON.stringify(snapshot.val());
                    var data = JSON.parse(json);
                    var html = '';
                    html += '<li>';
                    html += '<div class="chat-img"><img src="'+data.photourlSender+'" alt="user"></div>';
                    html += '<div class="chat-content">';
                    html += '<h5>'+data.usernameSender+'</h5>';
                    html += '<div class="box bg-light-primary">'+data.message+'</div>';
                    html += '</div>';
                    html += '<div class="chat-time">'+data.datemessage+'</div>';
                    html += '</li>';
                    $("#message-list").append(html);
                });
            });
            function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            }
        });


