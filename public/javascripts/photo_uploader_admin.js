$(document)
    .ready(
    function (e) {
        var url0, url1, url2, url3, url4;
        var ok = '1';
        var nok = '0';
        var userPhoto = 'https://lh4.googleusercontent.com/-CAh1em4rHtY/AAAAAAAAAAI/AAAAAAAAABo/tBCr1kRuvLA/s96-c/photo.jpg';
        var username = 'Modelic';
        var idsender = 'fVJGwG3B8VR0Jj8PaOM6gFGs2kC3';
        var config = {
            apiKey: "AIzaSyBsfwHz82n_ZM15OZPoWNpoFg73uUsHIHg",
            authDomain: "modelic-7b810.firebaseapp.com",
            databaseURL: "https://modelic-7b810.firebaseio.com",
            projectId: "modelic-7b810",
            storageBucket: "modelic-7b810.appspot.com",
            messagingSenderId: "737888261565"
        };
        firebase.initializeApp(config);
            var commentsRef = firebase.database().ref('commentaires/'+"test");
            var topUserInfos = firebase.database().ref('messages/'+idsender);
            var uploader=document.getElementById('uploader0'),
                uploader1=document.getElementById('uploader1'),
                fileButton0=document.getElementById('fileButton0'),
                fileButton1=document.getElementById('fileButton1'),
                fileButton2=document.getElementById('fileButton2'),
                fileButton3=document.getElementById('fileButton3'),
                password=document.getElementById('password-label');

    fileButton0.addEventListener('change', function(e) {
        var file=e.target.files[0];
        var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
        console.log(fileLocation);
        var task=storageRef.put(file);
        task.on('state_changed',
            function progress(snapshot){
                var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                uploader.value=percentage;
                console.log(snapshot.toString());
                if (percentage==100){
                    alert("file uploaded Successfully");
                }
            },
            function error(err){
                console.log("ERROR");
            },
            function complete(){
                var downloadURL = task.snapshot.downloadURL;
                console.log(downloadURL);
                url0 = downloadURL;
            }
        );
    });

    fileButton1.addEventListener('change', function(e) {
        var file=e.target.files[0];
        var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
        console.log(fileLocation);
        var task=storageRef.put(file);
        task.on('state_changed',
            function progress(snapshot){
                var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                uploader1.value=percentage;
                console.log(snapshot.toString());
                if (percentage==100){
                    alert("file uploaded Successfully");
                }
            },
            function error(err){
                console.log("ERROR");
            },
            function complete(){
                var downloadURL = task.snapshot.downloadURL;
                console.log(downloadURL);
                url1 = downloadURL;
            }
        );
    });

    fileButton2.addEventListener('change', function(e) {
        var file=e.target.files[0];
        var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
        console.log(fileLocation);
        var task=storageRef.put(file);
        task.on('state_changed',
            function progress(snapshot){
                var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                uploader2.value=percentage;
                console.log(snapshot.toString());
                if (percentage==100){
                    alert("file uploaded Successfully");
                }
            },
            function error(err){
                console.log("ERROR");
            },
            function complete(){
                var downloadURL = task.snapshot.downloadURL;
                console.log(downloadURL);
                url2 = downloadURL;
            }
        );
    });

    fileButton3.addEventListener('change', function(e) {
        var file=e.target.files[0];
        var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
        console.log(fileLocation);
        var task=storageRef.put(file);
        task.on('state_changed',
            function progress(snapshot){
                var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                uploader3.value=percentage;
                console.log(snapshot.toString());
                if (percentage==100){
                    alert("file uploaded Successfully");
                }
            },
            function error(err){
                console.log("ERROR");
            },
            function complete(){
                var downloadURL = task.snapshot.downloadURL;
                console.log(downloadURL);
                url3 = downloadURL;
            }
        );
    });

    function getPost(page) {
        $(".imload").fadeIn("1000");

        $(".imload").fadeIn("1000");
        $('#posts').html('');
        $('#pagingTop').html('');
        $('#pagingBottom').html('');
        $('#divPagination').empty();
        $('#divPagination').removeData("twbs-pagination");
        $('#divPagination').unbind("page");
        appRoutes.controllers.PostController.getAllPosts(page).ajax({
            success: function (data) {
                console.log(data);
                if (data.code == 200) {
                    if(data.posts.length != 0){
                        var posts = data.posts;
                        var numLine = (data.per_page * data.current_page) - data.per_page;
                        if (data.current_page == -1){numLine = 0}
                        for (var i in posts) {

                            var html = '';
                            numLine += 1;
                            html += '<tr>';
                            html += '<td>' + numLine + '</td>';
                            html += '<td>' + posts[i].author + '</td>';
                            html += '<td>' + posts[i].category + '</td>';
                            html += '<td>' + posts[i].texte + '</td>';
                            html += '<td>' + posts[i].titre + '</td>';
                            html += '<td>' + posts[i].sexe + '</td>';
                            html += '<td><a id ="' + posts[i].postid + '-' + posts[i].authorid + '-' + posts[i].author + posts[i].etat + '"href="#" class="btn btn-danger btn-xs delete-post">Supprimer</a></td>';
                            if(posts[i].etat == '0'){
                                html += '<td><a id ="' + posts[i].id + '-' + posts[i].authorid + '-' + posts[i].author + '"href="#" class="btn btn-success btn-xs validate-post">Valider</a></td>';
                            }else {
                                html += '<td><a id ="' + posts[i].id + '-' + posts[i].authorid + '-' + posts[i].author + posts[i].etat + '"href="#" class="btn btn-danger btn-xs unvalidate-post">Retirer</a></td>';
                            }
                            html += '<td><a id ="' + posts[i].id + '-' + posts[i].authorid + '-' + posts[i].author + '"href="#" class="btn btn-warning btn-xs update-post">Update</a></td>';
                            html += '</tr>';
                            $('#posts').append(html);

                        }

                        $('.image-popup-vertical-fit').magnificPopup({
                            type: 'image',
                            closeOnContentClick: true,
                            mainClass: 'mfp-img-mobile',
                            image: {
                                verticalFit: true
                            }

                        });

                        var current_page = data.current_page;
                        if(current_page == -1){current_page = 1}
                        $('#pagingTop').append(current_page + '/' + data.total_page + ' pages');
                        $('#pagingBottom').append(current_page + '/' + data.total_page + ' pages');
                        $('#divPaginationhaut').twbsPagination({
                            totalPages: data.total_page,
                            visiblePages: 3,
                            startPage: page,
                            onPageClick: function (event, numPage) {
                                page = numPage;
                                getPost(page);
                            }
                        });
                        $('#divPagination').twbsPagination({
                            totalPages: data.total_page,
                            visiblePages: 3,
                            startPage: page,
                            onPageClick: function (event, numPage2) {
                                page = numPage2;
                                getPost(page);
                            }
                        });
                    }else{
                        $('#voucherVide').html("Aucun voucher vendu");
                        $('#zoneRecherche').hide();
                    }

                } else if (data.result == "nok") {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
        });
    }

    $('#validate-post').click(function(){
        var authorPost = $('#author-post').val();
        var titrePost = $('#titre-post').val();
        var textePost = $('#texte-post').val();
        var category = $('#category-post').val();
        var telephone = $('#telephone-post').val();
        var sexe = $('#sexe-post').val();
        var jsonObj = {
            "author": authorPost,
            "authorid": "fVJGwG3B8VR0Jj8PaOM6gFGs2kC3",
            "titre": titrePost,
            "more":textePost,
            "photouser":"https://lh4.googleusercontent.com/-CAh1em4rHtY/AAAAAAAAAAI/AAAAAAAAABo/tBCr1kRuvLA/s96-c/photo.jpg",
            "pays":"Canada",
            "category":category,
            "datepost":"",
            "telephone":telephone,
            "sexe":sexe,
            "url0":url0,
            "url1":url1,
            "url2":url2,
            "url3":url3
        };
        if (url0!= null || url1!=null  || url2!=null || url3!=null || url4!=null){
//                if (!titrePost.trim()){
//                    alert("Titre obligatoire")
//                }
//                if (!textePost.trim()){
//                    alert("Texte obligatoire")
//                }
//                if (titrePost.trim() && textePost.trim()){
//                    doPost(jsonObj);
//                }
            doPost(jsonObj);
        }else {
            alert("Au moins une photo obligatoire")
        }
    });

    function doPost(data) {
        $(".imload").fadeIn("1000");
        appRoutes.controllers.PostController.doPost(data).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success : function (data){
                if (data.code == 200) {
                    $('#exampleModal').modal("hide");
                    console.log(data.message);
                    getPost();
                } else {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
        });
    }
});

