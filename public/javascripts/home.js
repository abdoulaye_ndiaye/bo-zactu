$(document)
    .ready(
    function (e) {
        var idtosend = getUrlParameter('id');
        var userPhoto = getUrlParameter('url');
        var username = getUrlParameter('user');
        var idsender = $(".user-informations").attr('id');
        var config = {
            apiKey: "AIzaSyBsfwHz82n_ZM15OZPoWNpoFg73uUsHIHg",
            authDomain: "modelic-7b810.firebaseapp.com",
            databaseURL: "https://modelic-7b810.firebaseio.com",
            projectId: "modelic-7b810",
            storageBucket: "modelic-7b810.appspot.com",
            messagingSenderId: "737888261565"
        };
        firebase.initializeApp(config);
        var commentsRef = firebase.database().ref('commentaires/'+"test");
        var topUserInfos = firebase.database().ref('messages/'+idsender);
        var uploader=document.getElementById('uploader0'),
            fileButton=document.getElementById('fileButton0'),
            uploader1=document.getElementById('uploader1'),
            fileButton1=document.getElementById('fileButton1'),
            password=document.getElementById('password-label'),
            username=document.getElementById('user-name-label'),
            btnLogin=document.getElementById('btn-login'),
            btnSignUp=document.getElementById('btn-signup'),
            facebookLogin=document.getElementById('facebook-login'),
            googleLogin=document.getElementById('google-login');

        var googleProvider = new firebase.auth.GoogleAuthProvider();
        var facebookProvider = new firebase.auth.FacebookAuthProvider();
        var html = '';
        var curr_page = 1;
        var category = getUrlParameter('category');
        var sexe = getUrlParameter('sexe');
        if (sexe == "homme"){
            $(".btn-tocheck").removeClass("active");
            $("#sexe-homme").addClass("active");
        }
        if (sexe == "femme"){
            $(".btn-tocheck").removeClass("active");
            $("#sexe-femme").addClass("active");
        }
        $('#posts').html('');
        initPageElements();
        function getPost(category, sexe, page) {

            $(".imload").fadeIn("1000");
            $('#pagingTop').html('');
            $('#pagingBottom').html('');
            $('#divPagination').empty();
            $('#divPagination').removeData("twbs-pagination");
            $('#divPagination').unbind("page");
            appRoutes.controllers.PostController.getValidePosts(category, sexe, page).ajax({
                success: function (data) {
                $(".preloader").fadeOut();
                    console.log(data);
                    if (data.code == 200) {
                        if(data.posts.length != 0){
                            var posts = data.posts;
                            var numLine = (data.per_page * data.current_page) - data.per_page;
                            if (data.current_page == -1){numLine = 0}
                            for (var i in posts) {

                                var html = '';
                                numLine += 1;
                                html += '<div class="card">';
                                    html += '<div class="el-card-item">';
                                        html += '<div class="el-card-avatar el-overlay-1">';
                                            // html += '<a id="'+posts[i].urls[0]+'_mod'+posts[i].urls[1]+'_mod'+posts[i].urls[2]+'_mod'+posts[i].urls[3]+'" class="image-popup-vertical-fit" href="#"> <img src="'+posts[i].urls[0]+'" alt="user" /> </a>';
                                            html += '<a id="'+posts[i].postid+'_'+posts[i].category+'_'+posts[i].sexe+'" class="image-popup-vertical-fit" href="#"> <img src="'+posts[i].urls[0]+'" alt="user" /> </a>';
                                        html += '</div>';
                                        html += '<div class="el-card-content">';
                                            if (posts[i].authorPhoto != null){
                                                html +='<a id="'+posts[i].authorid+'" href="#" class="img-profil"><img src="'+posts[i].authorPhoto+'" alt="user" width="40" class="img-circle"></a>';
                                            }
                                            html += '<h5 class="box-title">'+posts[i].titre+'</h5> <small>'+posts[i].texte+'</small>';
                                            html += '<br/>';
                                        html += '</div>';
                                    html += '</div>';
                                    html += '<div class="row">';
                                        html += '<div class="col-md-4">';
                                            html += '<button id="'+posts[i].authorid+'_'+posts[i].authorPhoto+'_'+posts[i].author+'" class="send-message btn btn-outline-info waves-effect btn-circle waves-light" type="button"> <i class="fa fa-facebook"></i> </button>';
                                        html += '</div>';
                                        html += '<div class="col-md-4">';
                                            html += '<button id="'+posts[i].postid+'elt'+posts[i].urls[0]+'" class="btn btn-outline-danger waves-effect btn-circle waves-light modal-comment" alt="default" data-toggle="modal" data-target="#myModal"> <i class="fa fa-facebook"></i> </button>';
                                        html += '</div>';
                                        html += '<div class="col-md-4">';
                                            html += '<button class="btn btn-outline-danger waves-effect btn-circle waves-light" type="button"> <i class="fa fa-facebook"></i> </button>';
                                        html += '</div>';
                                    html += '</div>';
                                html += '</div>';

                                $('#posts').append(html);

                            }

                            $(".image-popup-vertical-fit").click(function(){
                                var id = $(this).attr('id');
                                var sp = id.split('_');
                                $(location).attr('href', "/details?id="+sp[0]+"&category="+sp[1]+"&sexe="+sp[2]);
                            });

                            $(".img-profil").click(function(){
                                var id = $(this).attr('id');
                                $(location).attr('href', "/profil?id="+id);
                            });

                            $(".modal-comment").click(function(){
                                $("#message-list").html('');
                                var id = $(this).attr('id');
                                var sp = id.split('elt');
                                commentsRef = firebase.database().ref('commentaires/'+sp[0]);
                                commentsRef.on('child_added', function(snapshot) {
                                    var json = JSON.stringify(snapshot.val());
                                    var data = JSON.parse(json);
                                    var html = '';
                                    html += '<li>';
                                    html += '<div class="chat-img"><img src="'+data.photourlSender+'" alt="user"></div>';
                                    html += '<div class="chat-content">';
                                    html += '<h5>'+data.usernameSender+'</h5>';
                                    html += '<div class="box bg-light-primary">'+data.commentaire+'</div>';
                                    html += '</div>';
                                    html += '<div class="chat-time">'+data.datecommentaire+'</div>';
                                    html += '</li>';
                                    $("#message-list").append(html);
                                    console.log("source "+sp[1])
                                    $("#img-tocomment").attr("src", sp[1]);
                                });
                            });

                            $("#send-comment").click(function(){
                                var newPostRef = commentsRef.push();
                                var date = new Date().getDate();
                                var year = new Date().getFullYear();
                                var time = new Date().getTime();
                                newPostRef.set({
                                    "commentaire": $("#message-content").val(),
                                    "datecommentaire": date+"-"+year+"-"+time,
                                    "photourlSender": $(".img-user").attr('id'),
                                    "usernameSender": $("#user-name-id").text()
                                });
                                $("#message-content").val('');
                            });

                            $(".send-message").click(function(){
                                var id1 = $(".user-informations").attr('id');
                                var id = $(this).attr('id');
                                var sp = id.split('_');
                                $(location).attr('href', "/message?id="+sp[0]+"&url="+sp[1]+"&user="+sp[2]);
                            });
                        }else{
                            curr_page = 0;
                            $('#voucherVide').html("Oups!Service indisponible");
                            $('#zoneRecherche').hide();
                        }

                    } else if (data.result == "nok") {
                        alert(data.message);
                    }

                    $(".imload").fadeOut("1000");

                    $('.delete-post').click(function () {
                       var idd = $(this).attr('id');
                       console.log(idd);
                       var spp = idd.split('-');
                       var idPost = spp[0];
                       deletePost(idPost);
                    });

                    $('.validate-post').click(function () {
                       var idd = $(this).attr('id');
                       console.log(idd);
                       var spp = idd.split('-');
                       var idPost = spp[0];
                       validatePost(idPost);
                    });
                },
                error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                    if (objetExeption == "Unauthorized") {
                        $(location).attr('href', "/");
                    }
                    $(".imload").fadeOut("1000");
                }
            });
        }

        topUserInfos.on('child_added', function(snapshot) {
            $("#chat-sender").html('');
            var thisid = snapshot.key;
            snapshot.forEach(function(childSnapshot) {
             var json = JSON.stringify(childSnapshot.val());
             var data = JSON.parse(json);
             var html = '';
             html += '<a href="#">';
                 html += '<div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>';
                 html += '<div class="mail-contnet">';
                 html += '<h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>';
             html += '</a>';

             $("#chat-sender").html(html);
            });
        });

        function initPageElements() {
            getPost(category, sexe);
        }

        $("#btn-search").click(function(){
            var searchParam = $("#input-search").val();
            $('#posts').html('');
            getModelsAfterSeach(searchParam);
        });

        function getModelsAfterSeach(data, page) {
            $('#pagingTop').html('');
            $('#pagingBottom').html('');
            $('#divPagination').empty();
            $('#divPagination').removeData("twbs-pagination");
            $('#divPagination').unbind("page");
            appRoutes.controllers.PostController.getValidePostSearch(data, page).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success: function (data) {
                $(".preloader").fadeOut();
                    console.log(data);
                    if (data.code == 200) {
                        if(data.posts.length != 0){
                            var posts = data.posts;
                            var numLine = (data.per_page * data.current_page) - data.per_page;
                            if (data.current_page == -1){numLine = 0}
                            for (var i in posts) {

                                var html = '';
                                numLine += 1;
                                html += '<div class="card">';
                                    html += '<div class="el-card-item">';
                                        html += '<div class="el-card-avatar el-overlay-1">';
                                            // html += '<a id="'+posts[i].urls[0]+'_mod'+posts[i].urls[1]+'_mod'+posts[i].urls[2]+'_mod'+posts[i].urls[3]+'" class="image-popup-vertical-fit" href="#"> <img src="'+posts[i].urls[0]+'" alt="user" /> </a>';
                                            html += '<a id="'+posts[i].postid+'_'+posts[i].category+'_'+posts[i].sexe+'" class="image-popup-vertical-fit" href="#"> <img src="'+posts[i].urls[0]+'" alt="user" /> </a>';
                                        html += '</div>';
                                        html += '<div class="el-card-content">';
                                            if (posts[i].authorPhoto != null){
                                                html +='<a id="'+posts[i].authorid+'" href="#" class="img-profil"><img src="'+posts[i].authorPhoto+'" alt="user" width="40" class="img-circle"></a>';
                                            }
                                            html += '<h5 class="box-title">'+posts[i].titre+'</h5> <small>'+posts[i].texte+'</small>';
                                            html += '<br/>';
                                        html += '</div>';
                                    html += '</div>';
                                    html += '<div class="row">';
                                        html += '<div class="col-md-4">';
                                            html += '<button id="'+posts[i].authorid+'_'+posts[i].authorPhoto+'_'+posts[i].author+'" class="send-message btn btn-outline-info waves-effect btn-circle waves-light" type="button"> <i class="fa fa-facebook"></i> </button>';
                                        html += '</div>';
                                        html += '<div class="col-md-4">';
                                            html += '<button id="'+posts[i].postid+'elt'+posts[i].urls[0]+'" class="btn btn-outline-danger waves-effect btn-circle waves-light modal-comment" alt="default" data-toggle="modal" data-target="#myModal"> <i class="fa fa-facebook"></i> </button>';
                                        html += '</div>';
                                        html += '<div class="col-md-4">';
                                            html += '<button class="btn btn-outline-danger waves-effect btn-circle waves-light" type="button"> <i class="fa fa-facebook"></i> </button>';
                                        html += '</div>';
                                    html += '</div>';
                                html += '</div>';

                                $('#posts').append(html);

                            }

                            $(".image-popup-vertical-fit").click(function(){
                                var id = $(this).attr('id');
                                var sp = id.split('_');
                                $(location).attr('href', "/details?id="+sp[0]+"&category="+sp[1]+"&sexe="+sp[2]);
                            });

                            $(".img-profil").click(function(){
                                var id = $(this).attr('id');
                                $(location).attr('href', "/profil?id="+id);
                            });

                            $(".modal-comment").click(function(){
                                $("#message-list").html('');
                                var id = $(this).attr('id');
                                var sp = id.split('elt');
                                commentsRef = firebase.database().ref('commentaires/'+sp[0]);
                                commentsRef.on('child_added', function(snapshot) {
                                    var json = JSON.stringify(snapshot.val());
                                    var data = JSON.parse(json);
                                    var html = '';
                                    html += '<li>';
                                    html += '<div class="chat-img"><img src="'+data.photourlSender+'" alt="user"></div>';
                                    html += '<div class="chat-content">';
                                    html += '<h5>'+data.usernameSender+'</h5>';
                                    html += '<div class="box bg-light-primary">'+data.commentaire+'</div>';
                                    html += '</div>';
                                    html += '<div class="chat-time">'+data.datecommentaire+'</div>';
                                    html += '</li>';
                                    $("#message-list").append(html);
                                    console.log("source "+sp[1])
                                    $("#img-tocomment").attr("src", sp[1]);
                                });
                            });

                            $("#send-comment").click(function(){
                                var newPostRef = commentsRef.push();
                                var date = new Date().getDate();
                                var year = new Date().getFullYear();
                                var time = new Date().getTime();
                                newPostRef.set({
                                    "commentaire": $("#message-content").val(),
                                    "datecommentaire": date+"-"+year+"-"+time,
                                    "photourlSender": $(".img-user").attr('id'),
                                    "usernameSender": $("#user-name-id").text()
                                });
                                $("#message-content").val('');
                            });

                            $(".send-message").click(function(){
                                var id1 = $(".user-informations").attr('id');
                                var id = $(this).attr('id');
                                var sp = id.split('_');
                                $(location).attr('href', "/message?id="+sp[0]+"&url="+sp[1]+"&user="+sp[2]);
                            });
                        }else{
                            curr_page = 0;
                            $('#voucherVide').html("Oups!Service indisponible");
                            $('#zoneRecherche').hide();
                        }

                    } else if (data.result == "nok") {
                        alert(data.message);
                    }

                    $(".imload").fadeOut("1000");

                    $('.delete-post').click(function () {
                       var idd = $(this).attr('id');
                       console.log(idd);
                       var spp = idd.split('-');
                       var idPost = spp[0];
                       deletePost(idPost);
                    });

                    $('.validate-post').click(function () {
                       var idd = $(this).attr('id');
                       console.log(idd);
                       var spp = idd.split('-');
                       var idPost = spp[0];
                       validatePost(idPost);
                    });
                },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
          });
        }

        function deletePost(data) {
            $(".imload").fadeIn("1000");
            appRoutes.controllers.PostController.deletePost(data).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success : function (data){
            console.log(data)
                if (data.code == 200) {
                    alert(data.message);
                    getPost();
                } else {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
          });
        }

        function validatePost(data) {
            $(".imload").fadeIn("1000");
            appRoutes.controllers.PostController.validatePost(data).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success : function (data){
            console.log(data)
                if (data.code == 200) {
                    alert(data.message);
                    getPost();
                } else {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
          });
        }

        $("#bt_reche_reglement").click(function () {
           var idTransaction = $('#idTransaction').val();
           var idVoucher = $('#idVoucher').val();
           var jsonObj = {
                      "idtransaction": idTransaction,
                      "idvoucher":idVoucher
                     };

           getReglementAfterSeach(jsonObj);
        });

        $('p').on('click', '.del-element', function() {
            var text = $(this).text();
            var id = text.toLowerCase();
            id = id.replace(/ /g,"");
            $('#'+ id +'').fadeIn();
            $(this).next().remove();
            $(this).next().remove();
            $(this).fadeOut();
        });

        $('.del-sodeci').click(function(){
           $('#typefacture').fadeIn();
           $(this).next().remove();
           $(this).next().remove();
           $(this).fadeOut();
        });

        $("#menu_accueil").click(function(){
            $(location).attr('href', "/articles");
            $("#btn-tocheck").removeClass("active");
            $("#no-sexe").addClass("active");
        });

        $("#no-sexe").click(function(){
            $(location).attr('href', "/articles");
        });

        $("#menu_vetement").click(function(){
            category = "vêtements";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-bijoux").click(function(){
            category = "bijoux";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-coiffure").click(function(){
            category = "coiffures";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-sac").click(function(){
            category = "sacs";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-chassure").click(function(){
            category = "chaussures";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#sexe-homme").click(function(){
            $(location).attr('href', "/articles?category="+category+"&sexe="+"homme");
        });

        $("#sexe-femme").click(function(){
            $(location).attr('href', "/articles?category="+category+"&sexe="+"femme");
        });

        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        $(window).on("scroll", function() {
            var scrollHeight = $(document).height();
            var scrollPosition = $(window).height() + $(window).scrollTop();
            if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                if (curr_page != 0){
                    curr_page = curr_page+1;
                    getPost(category, sexe, curr_page);
                }
            }
        });

        fileButton.addEventListener('change', function(e) {
            var file=e.target.files[0];
            var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
            console.log(fileLocation);
            var task=storageRef.put(file);
            task.on('state_changed',
                function progress(snapshot){
                    var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                    uploader.value=percentage;
                    console.log(snapshot.toString());
                    if (percentage==100){
                        alert("file uploaded Successfully");
                    }
                },
                function error(err){
                    console.log("ERROR");
                },
                function complete(){
                    var downloadURL = task.snapshot.downloadURL;
                    console.log(downloadURL);
                    url0 = downloadURL;
                }
            );
        });

        fileButton1.addEventListener('change', function(e) {
            var file=e.target.files[0];
            var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
            console.log(fileLocation);
            var task=storageRef.put(file);
            task.on('state_changed',
                function progress(snapshot){
                    var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                    uploader1.value=percentage;
                    console.log(snapshot.toString());
                    if (percentage==100){
                        alert("file uploaded Successfully");
                    }
                },
                function error(err){
                    console.log("ERROR");
                },
                function complete(){
                    var downloadURL = task.snapshot.downloadURL;
                    console.log(downloadURL);
                    url1 = downloadURL;
                }
            );
        });

        fileButton2.addEventListener('change', function(e) {
            var file=e.target.files[0];
            var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
            console.log(fileLocation);
            var task=storageRef.put(file);
            task.on('state_changed',
                function progress(snapshot){
                    var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                    uploader2.value=percentage;
                    console.log(snapshot.toString());
                    if (percentage==100){
                        alert("file uploaded Successfully");
                    }
                },
                function error(err){
                    console.log("ERROR");
                },
                function complete(){
                    var downloadURL = task.snapshot.downloadURL;
                    console.log(downloadURL);
                    url2 = downloadURL;
                }
            );
        });

        fileButton3.addEventListener('change', function(e) {
            var file=e.target.files[0];
            var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
            console.log(fileLocation);
            var task=storageRef.put(file);
            task.on('state_changed',
                function progress(snapshot){
                    var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                    uploader3.value=percentage;
                    console.log(snapshot.toString());
                    if (percentage==100){
                        alert("file uploaded Successfully");
                    }
                },
                function error(err){
                    console.log("ERROR");
                },
                function complete(){
                    var downloadURL = task.snapshot.downloadURL;
                    console.log(downloadURL);
                    url3 = downloadURL;
                }
            );
        });

        fileButton4.addEventListener('change', function(e) {
            var file=e.target.files[0];
            var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
            console.log(fileLocation);
            var task=storageRef.put(file);
            task.on('state_changed',
                function progress(snapshot){
                    var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                    uploader4.value=percentage;
                    console.log(snapshot.toString());
                    if (percentage==100){
                        alert("file uploaded Successfully");
                    }
                },
                function error(err){
                    console.log("ERROR");
                },
                function complete(){
                    var downloadURL = task.snapshot.downloadURL;
                    console.log(downloadURL);
                    url4 = downloadURL;
                }
            );
        });

        btnLogin.addEventListener('click', function (e) {
            const em = username.value;
            const pwd = password.value;
            const auth = firebase.auth();
            const promise = auth.signInWithEmailAndPassword(em, pwd);
            console.log(promise.
            then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                // var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                console.log(user)
                window.location.assign("/articles")
                // ...
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                console.log(errorMessage);
                // ...
            }));
        });

        btnSignUp.addEventListener('click', function (e) {
            const em = username.value;
            const pwd = password.value;
            const auth = firebase.auth();
            const promise = auth.createUserWithEmailAndPassword(em, pwd);
            console.log(promise.
            then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                console.log(user);
                window.location.assign("/articles")

                // ...
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                // var credential = error.credential;
                console.log(errorMessage);
                // ...
            }));
        });

        googleLogin.addEventListener('click', function(e){
            firebase.auth().signInWithPopup(googleProvider).then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                console.log(result);
                var jsonObj = {
                    "username": user.displayName,
                    "photoUrl": user.photoURL,
                    "uid": user.uid
                };
                saveSession(jsonObj);
                // window.location.assign("/articles")
                // ...
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                console.log(error);
                // ...
            });
        });

        facebookLogin.addEventListener('click', function(e){
            firebase.auth().signInWithPopup(facebookProvider).then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                console.log(result);
                var jsonObj = {
                    "username": user.displayName,
                    "photoUrl": user.providerData[0].photoURL,
                    "uid": user.uid
                };
                saveSession(jsonObj);
                // window.location.assign("/articles")
                // ...
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                console.log(error);
                // ...
            });
        });

        $('#validate-post').click(function(){
            var authorPost = $('#author-post').val();
            var titrePost = $('#titre-post').val();
            var textePost = $('#texte-post').val();
            var category = $('#category-post').val();
            var sexe = $('#sexe-post').val();
            var jsonObj = {
                "author": authorPost,
                "titre": titrePost,
                "texte":textePost,
                "category":category,
                "sexe":sexe,
                "url0":url0,
                "url1":url1,
                "url2":url2,
                "url3":url3,
                "url4":url4
            };
            if (url0!= null || url1!=null  || url2!=null || url3!=null || url4!=null){
                if (!titrePost.trim()){
                    alert("Titre obligatoire")
                }
                if (!textePost.trim()){
                    alert("Texte obligatoire")
                }
                if (titrePost.trim() && textePost.trim()){
                    validatePost(jsonObj);
                }
            }else {
                alert("Au moins une photo obligatoire")
            }
        });

        function validatePost(data) {
            $(".imload").fadeIn("1000");
            appRoutes.controllers.PostController.doPost(data).ajax({
                data : JSON.stringify(data),
                contentType : 'application/json',
                success : function (data){
                    console.log(data)
                    if (data.code == 200) {
                        $('#myModal').modal("hide");
                        alert(data.message);
                    } else {
                        alert(data.message);
                    }

                    $(".imload").fadeOut("1000");
                },
                error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                    if (objetExeption == "Unauthorized") {
                        $(location).attr('href', "/");
                    }
                    $(".imload").fadeOut("1000");
                }
            });
        }

        function saveSession(data) {
            $(".imload").fadeIn("1000");
            appRoutes.controllers.UtilisateurController.storeToSession(data).ajax({
                data : JSON.stringify(data),
                contentType : 'application/json',
                success : function (data){
                    console.log(data)
                    if (data.code == 200) {
                        $('#myModal').modal("hide");
//                        alert(data.message);
                        window.location.assign("/articles")
                    } else {
                        alert(data.message);
                    }

                    $(".imload").fadeOut("1000");
                },
                error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                    if (objetExeption == "Unauthorized") {
                        $(location).attr('href', "/");
                    }
                    $(".imload").fadeOut("1000");
                }
            });
        }
});

