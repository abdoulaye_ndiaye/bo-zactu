$(document)
    .ready(
    function (e) {
        var html = '';
        var curr_page = 1;
        var category = getUrlParameter('category');
        var sexe = getUrlParameter('sexe');
        $('#photo-user').html('');
        $('#posts').html('');
        var url = window.location.search;
        var iduser = url.substring(url.lastIndexOf("=")+1);
        initPageElements();
        function getUserInfos(iduser) {
        $(".imload").fadeIn("1000");

            $(".imload").fadeIn("1000");
            $('#photo-user').html('');
            $('#pagingTop').html('');
            $('#pagingBottom').html('');
            $('#divPagination').empty();
            $('#divPagination').removeData("twbs-pagination");
            $('#divPagination').unbind("page");
            appRoutes.controllers.UtilisateurController.getUserInfos(iduser).ajax({
                success: function (data) {
                    console.log(data);
                    if (data.code == 200) {
                        if(data.users.length != 0){
                            curr_page = curr_page+1;
                            var users = data.users;
                            var numLine = (data.per_page * data.current_page) - data.per_page;
                            if (data.current_page == -1){numLine = 0}
                            for (var i in users) {
                                if (users[i].email === "null" || users[i].email === '' || users[i].email === "undefined"){
                                    $('#id-email-adress').html("Non renseigné");
                                }else {
                                    $('#id-email-adress').html(""+users[i].email);
                                }if (users[i].telephone === "null" || users[i].telephone === '' || users[i].telephone === "undefined"){
                                    $('#id-phone').html("Non renseigné");
                                }else {
                                    $('#id-phone').html(""+users[i].telephone);
                                }if (users[i].adresse === "null" || users[i].adresse === '' || users[i].adresse === "undefined"){
                                    $('#id-adress').html("Non renseigné");
                                }else {
                                    $('#id-adress').html(""+users[i].adresse);
                                }
                                if (users[i].nom === "null" || users[i].nom === '' || users[i].nom === "undefined"){
                                    $('#id-full-name').html("Non renseigné");
                                }else {
                                    $('#id-full-name').html(""+users[i].nom);
                                }
                                if (users[i].telephone === "null" || users[i].telephone === '' || users[i].telephone === "undefined"){
                                    $('#id-mobile').html("Non renseigné");
                                }else {
                                    $('#id-mobile').html(""+users[i].telephone);
                                }
                                if (users[i].email === "null" || users[i].email === '' || users[i].email === "undefined"){
                                    $('#id-email').html("Non renseigné");
                                }else {
                                    $('#id-email').html(""+users[i].email);
                                }
                                if (users[i].pays === "null" || users[i].pays === '' || users[i].pays === "undefined"){
                                    $('#id-location').html("Non renseigné");
                                }else {
                                    $('#id-location').html(""+users[i].pays);
                                }
                                if (users[i].more === "null" || users[i].more === '' || users[i].more === "undefined"){
                                    $('#id-more').html("Non renseigné");
                                }else {
                                    $('#id-more').html(""+users[i].more);
                                }
                                if (users[i].nom === "null"){
                                    $('#id-fullname').val("Non renseigné");
                                }else {
                                    $('#id-fullname').val(""+users[i].nom);
                                }
                                if (users[i].adresse === "null" || users[i].adresse === '' || users[i].adresse === "undefined"){
                                    $('#adresse-user').val("Non renseigné");
                                }else {
                                    $('#adresse-user').val(""+users[i].adresse);
                                }
                                if (users[i].email === "null" || users[i].email === '' || users[i].email === "undefined"){
                                    $('#email-user').val("Non renseigné");
                                }else {
                                    $('#email-user').val(""+users[i].email);
                                }
                                if (users[i].telephone === "null" || users[i].telephone === '' || users[i].telephone === "undefined"){
                                    $('#phone-user').val("Non renseigné");
                                }else {
                                    $('#phone-user').val(""+users[i].telephone);
                                }
                                if (users[i].more === "null" || users[i].more === '' || users[i].more === "undefined"){
                                    $('#more-user').val("Non renseigné");
                                }else {
                                    $('#more-user').val(""+users[i].more);
                                }
                                var html = '';
                                html += '<center class="m-t-30"> <img src="'+users[i].ppurl+'" class="img-circle" width="150" />';
                                    html += '<h4 class="card-title m-t-10">'+users[i].nom+'</h4>';
                                    html += '<h6 class="card-subtitle">Accoubts Manager Amix corp</h6>';
                                    html += '<div class="row text-center justify-content-md-center">';
                                        html += '<div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254</font></a></div>';
                                    html += '<br/>';
                                html += '</center>';
                                $('#photo-user').append(html);
                            }

                            $(".image-popup-vertical-fit").click(function(){
                                var id = $(this).attr('id');
                                var sp = id.split('_');
                                $(location).attr('href', "/details?id="+sp[0]+"&category="+sp[1]+"&sexe="+sp[2]);
                            });

                        }else{
                            curr_page = 0;
                            $('#voucherVide').html("Aucun voucher vendu");
                            $('#zoneRecherche').hide();
                        }

                    } else if (data.result == "nok") {
                        alert(data.message);
                    }

                    $(".imload").fadeOut("1000");
                },
                error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                    if (objetExeption == "Unauthorized") {
                        $(location).attr('href', "/");
                    }
                    $(".imload").fadeOut("1000");
                }
            });
        }

        function getUserPosts(iduser) {
            $(".imload").fadeIn("1000");

            $(".imload").fadeIn("1000");
            $('#pagingTop').html('');
            $('#pagingBottom').html('');
            $('#divPagination').empty();
            $('#divPagination').removeData("twbs-pagination");
            $('#divPagination').unbind("page");
            appRoutes.controllers.PostController.getUserPosts(iduser).ajax({
                success: function (data) {
                    console.log(data);
                    if (data.code == 200) {
                        if(data.posts.length != 0){
                            curr_page = curr_page+1;
                            var posts = data.posts;
                            var numLine = (data.per_page * data.current_page) - data.per_page;
                            if (data.current_page == -1){numLine = 0}
                            for (var i in posts) {
                                var html = '';
                                numLine += 1;
                                html += '<div class="card">';
                                    html += '<div class="el-card-item">';
                                        html += '<div class="el-card-avatar el-overlay-1">';
                                            html += '<a class="image-popup-vertical-fit" href="'+posts[i].urls[0]+'"> <img src="'+posts[i].urls[0]+'" alt="user" /> </a>';
                                        html += '</div>';
                                        html += '<div class="el-card-content">';
                                            html += '<h3 class="box-title">'+posts[i].titre+'</h3> <small>'+posts[i].texte+'</small>';
                                            html += '<br/>';
                                        html += '</div>';
                                    html += '</div>';
                                html += '</div>';

                                $('#posts').append(html);
                                $('#id-email-adress').html();
                            }

                            $('.image-popup-vertical-fit').magnificPopup({
                                type: 'image',
                                closeOnContentClick: true,
                                mainClass: 'mfp-img-mobile',
                                image: {
                                    verticalFit: true
                                 }
                            });

                            // $(".image-popup-vertical-fit").click(function(){
                            //     var id = $(this).attr('id');
                            //     var sp = id.split('_');
                            //     $(location).attr('href', "/details?id="+sp[0]+"&category="+sp[1]+"&sexe="+sp[2]);
                            // });

                        }else{
                            curr_page = 0;
                            $('#voucherVide').html("Aucun voucher vendu");
                            $('#zoneRecherche').hide();
                        }

                    } else if (data.result == "nok") {
                        alert(data.message);
                    }

                    $(".imload").fadeOut("1000");
                },
                error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                    if (objetExeption == "Unauthorized") {
                        $(location).attr('href', "/");
                    }
                    $(".imload").fadeOut("1000");
                }
            });
        }

        function validateUpdate(data) {
            $(".imload").fadeIn("1000");
            appRoutes.controllers.UtilisateurController.doPostUser(data).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success : function (data){
            console.log(data)
                if (data.code == 200) {
                    alert(data.message);
                    initPageElements();
                } else {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
          });
        }

        $("#update-profile").click(function () {
           var adresse = $('#adresse-user').val();
           var email = $('#email-user').val();
           var phone = $('#phone-user').val();
           var pays = $('#pays-user').val();
           var more = $('#more-user').val();
           var jsonObj = {
                      "adresse": adresse,
                      "email":email,
                      "phone":phone,
                      "more":more,
                      "pays":pays
                     };

            validateUpdate(jsonObj);
        });

        $('p').on('click', '.del-element', function() {
            var text = $(this).text();
            var id = text.toLowerCase();
            id = id.replace(/ /g,"");
            $('#'+ id +'').fadeIn();
            $(this).next().remove();
            $(this).next().remove();
            $(this).fadeOut();
        });

        $('.del-sodeci').click(function(){
           $('#typefacture').fadeIn();
           $(this).next().remove();
           $(this).next().remove();
           $(this).fadeOut();
        });

        function initPageElements() {

            $("#typefacture").fadeOut();
            $("#m_reglement").addClass("active");
            $("#filtre_date").val("");

            getUserInfos(iduser);
            getUserPosts(iduser);
        }

        $("#menu_vetement").click(function(){
            category = "vêtements";
            $(location).attr('href', "/articles?category="+category);
            // $(location).attr('href', "/articles?category="+category+"&sexe="+sp[2]);
        });

        $("#menu-bijoux").click(function(){
            category = "bijoux";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-coiffure").click(function(){
            category = "coiffures";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-sac").click(function(){
            category = "sacs";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-chassure").click(function(){
            category = "chaussures";
            $(location).attr('href', "/articles?category="+category);
        });

        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        $(window).on("scroll", function() {
            var scrollHeight = $(document).height();
            var scrollPosition = $(window).height() + $(window).scrollTop();
            if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                if (curr_page != 0){
                    getUserPosts(iduser, curr_page);
                }
            }
        });

});

