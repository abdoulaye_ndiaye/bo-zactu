$(document)
    .ready(
        function (e) {
            var url0, url1, url2, url3, url4;
            var ok = '1';
            var nok = '0';
            var config = {
                apiKey: "AIzaSyBsfwHz82n_ZM15OZPoWNpoFg73uUsHIHg",
                authDomain: "modelic-7b810.firebaseapp.com",
                databaseURL: "https://modelic-7b810.firebaseio.com",
                projectId: "modelic-7b810",
                storageBucket: "modelic-7b810.appspot.com",
                messagingSenderId: "737888261565"
            };
            firebase.initializeApp(config);
            var uploader=document.getElementById('uploader0'),
                fileButton=document.getElementById('fileButton0'),
                uploader1=document.getElementById('uploader1'),
                fileButton1=document.getElementById('fileButton1');

            fileButton.addEventListener('change', function(e) {
                var file=e.target.files[0];
                var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
                console.log(fileLocation);
                var task=storageRef.put(file);
                task.on('state_changed',
                    function progress(snapshot){
                        var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                        uploader.value=percentage;
                        console.log(snapshot.toString());
                        if (percentage==100){
                            alert("file uploaded Successfully");
                        }
                    },
                    function error(err){
                        console.log("ERROR");
                    },
                    function complete(){
                        var downloadURL = task.snapshot.downloadURL;
                        console.log(downloadURL);
                        url0 = downloadURL;
                    }
                );
            });

            fileButton1.addEventListener('change', function(e) {
                var file=e.target.files[0];
                var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
                console.log(fileLocation);
                var task=storageRef.put(file);
                task.on('state_changed',
                    function progress(snapshot){
                        var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                        uploader1.value=percentage;
                        console.log(snapshot.toString());
                        if (percentage==100){
                            alert("file uploaded Successfully");
                        }
                    },
                    function error(err){
                        console.log("ERROR");
                    },
                    function complete(){
                        var downloadURL = task.snapshot.downloadURL;
                        console.log(downloadURL);
                        url1 = downloadURL;
                    }
                );
            });

            fileButton2.addEventListener('change', function(e) {
                var file=e.target.files[0];
                var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
                console.log(fileLocation);
                var task=storageRef.put(file);
                task.on('state_changed',
                    function progress(snapshot){
                        var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                        uploader2.value=percentage;
                        console.log(snapshot.toString());
                        if (percentage==100){
                            alert("file uploaded Successfully");
                        }
                    },
                    function error(err){
                        console.log("ERROR");
                    },
                    function complete(){
                        var downloadURL = task.snapshot.downloadURL;
                        console.log(downloadURL);
                        url2 = downloadURL;
                    }
                );
            });

            fileButton3.addEventListener('change', function(e) {
                var file=e.target.files[0];
                var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
                console.log(fileLocation);
                var task=storageRef.put(file);
                task.on('state_changed',
                    function progress(snapshot){
                        var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                        uploader3.value=percentage;
                        console.log(snapshot.toString());
                        if (percentage==100){
                            alert("file uploaded Successfully");
                        }
                    },
                    function error(err){
                        console.log("ERROR");
                    },
                    function complete(){
                        var downloadURL = task.snapshot.downloadURL;
                        console.log(downloadURL);
                        url3 = downloadURL;
                    }
                );
            });

            fileButton4.addEventListener('change', function(e) {
                var file=e.target.files[0];
                var storageRef=firebase.storage().ref("'/fileLocation/'"+file.name);
                console.log(fileLocation);
                var task=storageRef.put(file);
                task.on('state_changed',
                    function progress(snapshot){
                        var percentage=( snapshot.bytesTransferred / snapshot.totalBytes )*100;
                        uploader4.value=percentage;
                        console.log(snapshot.toString());
                        if (percentage==100){
                            alert("file uploaded Successfully");
                        }
                    },
                    function error(err){
                        console.log("ERROR");
                    },
                    function complete(){
                        var downloadURL = task.snapshot.downloadURL;
                        console.log(downloadURL);
                        url4 = downloadURL;
                    }
                );
            });

            $('#validate-post').click(function(){
                var authorPost = $('#author-post').val();
                var titrePost = $('#titre-post').val();
                var textePost = $('#texte-post').val();
                var category = $('#category-post').val();
                var sexe = $('#sexe-post').val();
                var jsonObj = {
                    "author": authorPost,
                    "titre": titrePost,
                    "texte":textePost,
                    "category":category,
                    "sexe":sexe,
                    "url0":url0,
                    "url1":url1,
                    "url2":url2,
                    "url3":url3,
                    "url4":url4
                };
                if (url0!= null || url1!=null  || url2!=null || url3!=null || url4!=null){
                    if (!titrePost.trim()){
                        alert("Titre obligatoire")
                    }
                    if (!textePost.trim()){
                        alert("Texte obligatoire")
                    }
                    if (titrePost.trim() && textePost.trim()){
                        validatePost(jsonObj);
                    }
                }else {
                    alert("Au moins une photo obligatoire")
                }
            });

            function validatePost(data) {
                $(".imload").fadeIn("1000");
                appRoutes.controllers.PostController.doPost(data).ajax({
                    data : JSON.stringify(data),
                    contentType : 'application/json',
                    success : function (data){
                        console.log(data)
                        if (data.code == 200) {
                            $('#myModal').modal("hide");
                            alert(data.message);
                        } else {
                            alert(data.message);
                        }

                        $(".imload").fadeOut("1000");
                    },
                    error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                        if (objetExeption == "Unauthorized") {
                            $(location).attr('href', "/");
                        }
                        $(".imload").fadeOut("1000");
                    }
                });
            }

            function saveSession(data) {
                $(".imload").fadeIn("1000");
                appRoutes.controllers.UtilisateurController.storeToSession(data).ajax({
                    data : JSON.stringify(data),
                    contentType : 'application/json',
                    success : function (data){
                        console.log(data)
                        if (data.code == 200) {
                            $('#myModal').modal("hide");
                            alert(data.message);
                        } else {
                            alert(data.message);
                        }

                        $(".imload").fadeOut("1000");
                    },
                    error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                        if (objetExeption == "Unauthorized") {
                            $(location).attr('href', "/");
                        }
                        $(".imload").fadeOut("1000");
                    }
                });
            }
        });

