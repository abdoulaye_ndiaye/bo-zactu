$(document)
    .ready(
    function (e) {
        var category = getUrlParameter('category');
        var sexe = getUrlParameter('sexe');
        var curr_page = 1;
        $('#similar-posts').html('');
        initPageElements();
        var postId;
        function getPost(page) {
            $(".imload").fadeIn("1000");
            $('#posts').html('');
            postId = getUrlParameter('id');
            console.log(postId);
            appRoutes.controllers.PostController.getPostById(postId).ajax({
                success: function (data) {
                    console.log("getPost "+data);
                    if (data.code == 200) {
                        if(data.posts.length != 0){
                            var posts = data.posts;
                            var numLine = (data.per_page * data.current_page) - data.per_page;
                            if (data.current_page == -1){numLine = 0}
                            for (var i in posts) {
                                sexe = posts[i].sexe;
                                category = posts[i].category;
                                var html = '';
                                html += '<div class="col-md-3">';
                                    html += '<a href="'+posts[i].urls[i]+'" title="'+posts[i].texte+'"> <img src="'+posts[i].urls[i]+'" class="img-responsive" alt="img" /> </a>';
                                html += '</div>';
                                $('#details-post').append(html);
                            }

                            $(".image-popup-vertical-fit").click(function(){
                                var id = $(this).attr('id');
                                var sp = id.split('_mod');
                                $(location).attr('href', "/details?url1="+id);
                            });

                        }else{
                            $('#voucherVide').html("Aucun voucher vendu");
                            $('#zoneRecherche').hide();
                        }

                    } else if (data.result == "nok") {
                        alert(data.message);
                    }
                },
                error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                    if (objetExeption == "Unauthorized") {
                        $(location).attr('href', "/");
                    }
                    $(".imload").fadeOut("1000");
                }
            });
        }

        function getSimilarPost(id, category, sexe, page) {
            $(".imload").fadeIn("1000");
            // $('#similar-posts').html('');
            var id = getUrlParameter('id');
            console.log(id);
            appRoutes.controllers.PostController.getSimilarPosts(id, category, sexe, page).ajax({
                success: function (data) {
                    console.log("getSimilarPosts "+data);
                    if (data.code == 200) {
                        if(data.posts.length != 0){
                            var posts = data.posts;
                            var numLine = (data.per_page * data.current_page) - data.per_page;
                            if (data.current_page == -1){numLine = 0}
                            for (var i in posts) {
                                var html = '';
                                numLine += 1;
                                html += '<div class="card">';
                                    html += '<div class="el-card-item">';
                                        html += '<div class="el-card-avatar el-overlay-1">';
                                            // html += '<a id="'+posts[i].urls[0]+'_mod'+posts[i].urls[1]+'_mod'+posts[i].urls[2]+'_mod'+posts[i].urls[3]+'" class="image-popup-vertical-fit" href="#"> <img src="'+posts[i].urls[0]+'" alt="user" /> </a>';
                                            html += '<a id="'+posts[i].postid+'" class="image-popup-vertical-fit" href="#"> <img src="'+posts[i].urls[0]+'" alt="user" /> </a>';
                                        html += '</div>';
                                        html += '<div class="el-card-content">';
                                            html += '<h3 class="box-title">'+posts[i].titre+'</h3> <small>'+posts[i].texte+'</small>';
                                            html += '<br/>';
                                        html += '</div>';
                                    html += '</div>';
                                html += '</div>';

                                $('#similar-posts').append(html);
                            }

                            $(".image-popup-vertical-fit").click(function(){
                                var id = $(this).attr('id');
                                var sp = id.split('_');
                                $(location).attr('href', "/details?id="+sp[0]+"&category="+sp[1]+"&sexe="+sp[2]);
                            });

                        }else{
                            $('#voucherVide').html("Aucun article disponible");
                            $('#zoneRecherche').hide();
                        }

                    } else if (data.result == "nok") {
                        alert(data.message);
                    }
                },
                error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                    if (objetExeption == "Unauthorized") {
                        $(location).attr('href', "/");
                    }
                    $(".imload").fadeOut("1000");
                }
            });
        }

        function initPageElements() {
            $("#typefacture").fadeOut();
            $("#m_reglement").addClass("active");
            $("#filtre_date").val("");
            $('#details-post').html('');
            getPost();
            getSimilarPost(postId, category, sexe);
        }

        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        function deletePost(data) {
            $(".imload").fadeIn("1000");
            appRoutes.controllers.PostController.deletePost(data).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success : function (data){
            console.log(data)
                if (data.code == 200) {
                    alert(data.message);
                    getPost();
                } else {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
          });
        }

        function validatePost(data) {
            $(".imload").fadeIn("1000");
            appRoutes.controllers.PostController.validatePost(data).ajax({
            data : JSON.stringify(data),
            contentType : 'application/json',
            success : function (data){
            console.log(data)
                if (data.code == 200) {
                    alert(data.message);
                    getPost();
                } else {
                    alert(data.message);
                }

                $(".imload").fadeOut("1000");
            },
            error: function (xmlHttpReques, chaineRetourne, objetExeption) {
                if (objetExeption == "Unauthorized") {
                    $(location).attr('href', "/");
                }
                $(".imload").fadeOut("1000");
            }
          });
        }

        $("#menu_vetement").click(function(){
            category = "vêtements";
            $(location).attr('href', "/articles?category="+category);
            // $(location).attr('href', "/articles?category="+category+"&sexe="+sp[2]);
        });

        $("#menu-bijoux").click(function(){
            category = "bijoux";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-coiffure").click(function(){
            category = "coiffures";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-sac").click(function(){
            category = "sacs";
            $(location).attr('href', "/articles?category="+category);
        });

        $("#menu-chassure").click(function(){
            category = "chaussures";
            $(location).attr('href', "/articles?category="+category);
        });

        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        $(window).on("scroll", function() {
            var scrollHeight = $(document).height();
            var scrollPosition = $(window).height() + $(window).scrollTop();
            if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                if (curr_page != 0){
                    curr_page = curr_page+1;
                    getSimilarPost(postId, category, sexe, curr_page);
                }
            }
        });
});

