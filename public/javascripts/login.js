$(document)
    .ready(
    function (e) {
        var url0, url1, url2, url3, url4;
        var config = {
            apiKey: "AIzaSyBsfwHz82n_ZM15OZPoWNpoFg73uUsHIHg",
            authDomain: "modelic-7b810.firebaseapp.com",
            databaseURL: "https://modelic-7b810.firebaseio.com",
            projectId: "modelic-7b810",
            storageBucket: "modelic-7b810.appspot.com",
            messagingSenderId: "737888261565"
        };
        firebase.initializeApp(config);
        var uploader=document.getElementById('uploader0'),
            fileButton=document.getElementById('fileButton0'),
            uploader1=document.getElementById('uploader1'),
            fileButton1=document.getElementById('fileButton1'),
            password=document.getElementById('password-label'),
            username=document.getElementById('user-name-label'),
            btnLogin=document.getElementById('btn-login');
            btnSignUp=document.getElementById('btn-signup');
            facebookLogin=document.getElementById('facebook-login');
            googleLogin=document.getElementById('google-login');

        var googleProvider = new firebase.auth.GoogleAuthProvider();
        var facebookProvider = new firebase.auth.FacebookAuthProvider();

        btnLogin.addEventListener('click', function (e) {
            const em = username.value;
            const pwd = password.value;
            const auth = firebase.auth();
            const promise = auth.signInWithEmailAndPassword(em, pwd);
            console.log(promise.
            then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                // var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                console.log(result.email)
                // ...
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                console.log(errorMessage);
                // ...
            }));
        });

        btnSignUp.addEventListener('click', function (e) {
            const em = username.value;
            const pwd = password.value;
            const auth = firebase.auth();
            const promise = auth.createUserWithEmailAndPassword(em, pwd);
            console.log(promise.
            then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                console.log(user)
                // ...
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                // var credential = error.credential;
                console.log(errorMessage);
                // ...
            }));
        });

        googleLogin.addEventListener('click', function(e){
            firebase.auth().signInWithPopup(googleProvider).then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                console.log(result);
                // ...
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                console.log(error);
                // ...
            });
        });

        facebookLogin.addEventListener('click', function(e){
            firebase.auth().signInWithPopup(facebookProvider).then(function(result) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // The signed-in user info.
                var user = result.user;
                console.log(result);
                // ...
            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // The email of the user's account used.
                var email = error.email;
                // The firebase.auth.AuthCredential type that was used.
                var credential = error.credential;
                console.log(error);
                // ...
            });
        });
});

