package dao;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Recherche;
import models.Utilisateur;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import play.Logger;
import play.db.DB;
import play.libs.Json;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Oumou on 06/05/2016.
 */
public class UtilisateurDaoImpl implements UtilisateurDAO, DefaultDAO<Utilisateur>{

    public static String iduser = "idutilisateur";

    public static String nom = "nom";

    public static String prenom = "prenom";

    public static String tel = "mobile";

    public static String datepwd = "datepws";

    public static String profil = "idprofil";

    public static String login = "login";

    @Override
    public ArrayList<Utilisateur> getListAll(int numeroPage, int parPage, boolean all) throws SQLException {

        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder request = new StringBuilder("SELECT iduser,prenom,nom,etat,p.idprofil,login,password," +
                "isconnected,nbrtentative,email,telephone,DATE(datecreation),DATE(datemodification),DATE(datepwd)," +
                "codeprofil,libelle " +
                "FROM users u, profil p " +
                "WHERE u.idprofil=p.idprofil");
        statement = c.createStatement();

        if (!all) {
            if (numeroPage == 1) {
                request.append(" LIMIT ").append(parPage);
            } else if (numeroPage > 1) {
                request.append(" LIMIT ").append((numeroPage - 1) * parPage).append(", ")
                        .append(parPage);
            } else {
                request.append(" LIMIT ").append(parPage);
            }
        }

        ResultSet resultSet = statement.executeQuery(request.toString());
        ArrayList<Utilisateur> users = new ArrayList<>();
        try {
            statement = c.createStatement();
            resultSet = statement.executeQuery(request.toString());
            if (resultSet == null) {
                statement.close();
                resultSet.close();
                c.close();
                return users;
            }
            while (resultSet.next()) {
                Utilisateur u = new Utilisateur();
                u.setIdser(resultSet.getString("iduser"));
                u.setCodeprofil(resultSet.getString("codeprofil"));
                u.setPrenom(resultSet.getString("prenom"));
                u.setNom(resultSet.getString("nom"));
                u.setEtat(resultSet.getString("etat"));
                u.setIdprofil(resultSet.getString("idprofil"));
                u.setLogin(resultSet.getString("login"));
                u.setIsconnected(resultSet.getString("isconnected"));
                u.setNbrtentative(resultSet.getString("nbrtentative"));
                u.setEmail(resultSet.getString("email"));
                u.setTelephone(resultSet.getString("telephone"));
                u.setDatecreation(resultSet.getString("DATE(datecreation)"));
                u.setDatemodification(resultSet.getString("DATE(datemodification)"));
                u.setDatepwd(resultSet.getString("DATE(datepwd)"));
                u.setCodeprofil(resultSet.getString("codeprofil"));
                u.setLibelle(resultSet.getString("libelle"));
                users.add(u);
            }
            statement.close();
            resultSet.close();
            return users;

        } catch (SQLException e) {
            Logger.error(e.getMessage());
            statement.close();
            resultSet.close();
            return users;
        } finally {
            c.close();
        }
    }

    @Override
    public ArrayList<Utilisateur> getListAfterSeach(Recherche r, int numeroPage, int parPage, boolean all) throws SQLException {
        return null;
    }

    @Override
    public ArrayList<Utilisateur> connectUser(String login, String pwd) throws SQLException {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder request = new StringBuilder("SELECT * " +
                "FROM users, profil " +
                "WHERE users.idprofil=profil.idprofil " +
                "AND login='").append(login)
                .append("' and motdepasse='").append(pwd)
                .append("' AND verouiller !='0'");
        statement = c.createStatement();

        ResultSet resultSet = statement.executeQuery(request.toString());
        ArrayList<Utilisateur> users = new ArrayList<>();
        try {
            statement = c.createStatement();
            resultSet = statement.executeQuery(request.toString());
            if (!resultSet.next()) {
                statement.close();
                resultSet.close();
                c.close();
                return users;
            }
            Utilisateur u = new Utilisateur();
            u.setIdser(resultSet.getString("idutilisateur"));
            u.setIdprofil(resultSet.getString("idprofil"));
            u.setPrenom(resultSet.getString("prenom"));
            u.setNom(resultSet.getString("nom"));
            u.setLogin(resultSet.getString("login"));
            u.setPassword(resultSet.getString("motdepasse"));
            u.setEtat(resultSet.getString("verouiller"));

            UtilisateurDaoImpl.login= u.getLogin();
            UtilisateurDaoImpl.iduser= u.getIdser();
            UtilisateurDaoImpl.profil=u.getIdprofil();
            UtilisateurDaoImpl.nom=u.getNom();
            UtilisateurDaoImpl.prenom=u.getPrenom();
            users.add(u);
            statement.close();
            resultSet.close();
            return users;

        } catch (SQLException e) {
            statement.close();
            resultSet.close();
            return users;
        } finally {
            c.close();
        }
    }

    @Override
    public boolean updatePwdUser(String idUser, String mdp, String oldMdp) {
        Connection connection = DB.getConnection();
        PreparedStatement ps = null;
        String req;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date today = new java.util.Date();
            req = "UPDATE utilisateur SET password = ?,datepwd = ?,datemodification=? WHERE iduser = ? AND password = ?";
            ps = connection.prepareStatement(req);
            ps.setObject(1, mdp);
            ps.setObject(2, dateFormat.format(today));
            ps.setObject(3, dateFormat.format(today));
            ps.setObject(4, new Integer(idUser));
            ps.setObject(5, oldMdp);
            int retour = ps.executeUpdate();
            if (retour == 0) {
                return false;
            }
        } catch (SQLException e) {
            return false;
        } finally {
            try {
                ps.close();
                connection.close();
            } catch (SQLException e) {
            }
        }
        return true;
    }

    @Override
    public boolean addUtilisateur(String prenom, String nom, String login, String email, String tel, String profil, String generatedPass) throws SQLException {
        Connection connection = DB.getConnection();
        Statement stm = null;
        int userId = 0;
        DateTime now = new org.joda.time.DateTime();
        String pattern = "yyyy-MM-dd hh.mm.ss";
        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern(pattern);
        String datecreation = formatter.print(now);
        String req;

        stm = connection.createStatement();
        String request = "SELECT MAX(iduser) FROM utilisateur";
        ResultSet resultSet = stm.executeQuery(request);
        if (resultSet.next()) {
            userId=resultSet.getInt(1);
            userId = userId+1;
        }

        req = "INSERT INTO utilisateur(IDUSER,PRENOM,NOM,LOGIN,EMAIL,TELEPHONE,IDPROFIL,PASSWORD," +
                "DATECREATION,DATEMODIFICATION,DATEPWD) " +
                " VALUES ('"+ userId + "','" + prenom + "','" + nom + "' ,'" + login +"' ,'" + email +
                "' ,'" + tel +"' ,'" + profil+"' ,'" + generatedPass +"' ,'" + datecreation +"' ,'1971-01-01', '1971-01-01')";

        Logger.debug("REQ " + req);
        try {
            int retour = stm.executeUpdate(req);
            if (retour == 0) {
                Logger.info("RETOUR USER ==> " + retour);
                return false;
            }
            Logger.info("RETOUR STATEMENT ==> " + retour);
            Logger.info("req ==> " + req);

        } catch (SQLException e) {
            Logger.error("newUser SQLException " + e.getMessage());
            return false;
        } finally {
            try {
                stm.close();
                connection.close();

            } catch (SQLException e) {
                Logger.error("SQLExeption " + e.getMessage());
            }
        }
        return true;
    }

    @Override
    public ArrayList<Utilisateur> getProfilUser() throws SQLException {
        Connection c = DB.getConnection();
        Statement statement;
        ObjectNode result = Json.newObject();

        StringBuilder request = new StringBuilder("SELECT distinct idprofil, codeprofil, libelle FROM profil ");

        statement = c.createStatement();
        Logger.debug("la requete SQL " + request.toString());
        ResultSet resultSet = statement.executeQuery(request.toString());
        ArrayList<Utilisateur> profils = new ArrayList<>();

        try {
            while (resultSet.next()) {
                String code = resultSet.getString("codeprofil");
                String idProfil = resultSet.getString("idprofil");
                String profil = resultSet.getString("libelle");

                Utilisateur user = new Utilisateur(code, idProfil, profil);
                profils.add(user);
            }

            return profils;

        } catch (SQLException e) {
            Logger.error(e.getMessage());
            c.close();
            statement.close();
            resultSet.close();
            return profils;
        }finally {
            c.close();
        }
    }

    public static int getNbreTentative(String login)  throws SQLException {
        Connection c = DB.getConnection();
        Statement statement = null;
        int nbreTentative = 0;
        try {
            String req = "select nbrtentative from utilisateur WHERE login='" + login + "'";
            statement = c.createStatement();
            ResultSet resultSet = statement.executeQuery(req);
            while(resultSet.next()){
                nbreTentative = resultSet.getInt("nbrtentative");
            }
            return nbreTentative;
        } catch (SQLException e) {
            return nbreTentative;
        } finally {
            statement.close();
            c.close();
        }
    }

    public static boolean gestionConnexion(String req) throws SQLException {
        Connection c = DB.getConnection();
        Statement statement = null;
        try {
            statement = c.createStatement();
            int result = statement.executeUpdate(req);
            return result > 0;
        } catch (SQLException e) {
            return false;
        } finally {
            statement.close();
            c.close();
        }
    }
}
