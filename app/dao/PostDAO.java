package dao;

import models.PostModel;
import models.VenteModel;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by ghambyte on 9/3/16.
 */
public interface PostDAO {

    String AUTHOR = "author";

    String AUTHORID = "authorid";

    String ID = "id";

    String URL = "url";

    String POSTID = "postid";

    String MORE = "more";

    String PHOTOUSER = "photouser";

    String DATEPOST = "datepost";

    String CATEGORY = "category";

    String SEXE = "sexe";

    String PAYS = "pays";

    String TELEPHONE = "telephone";

    String ETAT = "etat";

    String POSTITRE = "posttitre";

    String POSTTEXTE = "posttexte";

    ArrayList<PostModel> getListPosts(String authorId, String category, String sexe, int page, int per_page);

    ArrayList<PostModel> getAllPosts(int page, int per_page, boolean all);

    ArrayList<PostModel> getPost(String id);

    ArrayList<String> getPostPhotos(String id);

    ArrayList<VenteModel> getPostVentes(String id);

    boolean doPost(PostModel postModel) throws SQLException;

    boolean deletePost(String id);

    boolean validatePost(String id, String etat);

    boolean updatePost(String id, PostModel postModel);

}
