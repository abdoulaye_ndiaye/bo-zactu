package dao;

import models.Recherche;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Oumou on 06/05/2016.
 */
public interface DefaultDAO<T> {
    ArrayList<T> getListAll(int numeroPage, int parPage, boolean all) throws SQLException;
    ArrayList<T> getListAfterSeach(Recherche r, int numeroPage, int parPage, boolean all) throws SQLException;
}
