package dao;

import models.Utilisateur;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Oumou on 06/05/2016.
 */
public interface UtilisateurDAO{
    ArrayList<Utilisateur> connectUser(String login, String pwd) throws SQLException;
    boolean updatePwdUser(String idUser, String mdp, String oldMdp);
    boolean addUtilisateur(String prenom, String nom, String login, String email,
                           String tel, String profil, String generatedPass) throws SQLException;
    ArrayList<Utilisateur> getProfilUser() throws SQLException;
}
