package dao;

import models.PostModel;
import models.VenteModel;
import play.Logger;
import play.db.DB;
import tools.Db;
import tools.Utils;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by ghambyte on 9/3/16.
 */
public class PostDAOImpl implements PostDAO {

    @Override
    public ArrayList<PostModel> getListPosts(String authorId, String category, String sexe, int page, int per_page){
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "SELECT * FROM posts as po, photos as ph WHERE po.id =ph.postid AND po.etat='0'");
        if (category != null && !category.isEmpty()){
            req.append("AND category='"+category+"'");
        }
        if (authorId != null && !authorId.isEmpty()){
            req.append("AND authorid='"+authorId+"'");
        }
        req.append(" ORDER BY id DESC");
        if (page == 1) {
            req.append(" LIMIT ").append(per_page);
        } else if (page > -1) {
            req.append(" LIMIT ").append(per_page).append(" OFFSET ")
                    .append((page - 1) * per_page);
        } else {
            req.append(" LIMIT ").append(per_page);
        }
        ArrayList<PostModel> models = new ArrayList<>();
        Logger.debug("getPostsList " + req.toString());
        ResultSet res;
        try {
            statement = c.createStatement();
            res = statement.executeQuery(req.toString());
            models = extractModel(res);
            return models;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return models;
        } finally {
            Db.closeQuietly(c);
        }
    }

    @Override
    public ArrayList<PostModel> getPost(String id) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "SELECT * FROM posts WHERE id ='"+ id +"'");

        ArrayList<PostModel> models = new ArrayList<>();
        Logger.debug("getPost " + req.toString());
        ResultSet res;
        try {
            statement = c.createStatement();
            res = statement.executeQuery(req.toString());
            models = extractModel(res);
            return models;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return models;
        } finally {
            Db.closeQuietly(c);
        }
    }

    @Override
    public boolean doPost(PostModel postModel) {
        Connection c = DB.getConnection();
        Statement statement;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String dateF = dateFormat.format(date);
        Iterator<String> iterator = postModel.getUrls().iterator();
        Iterator<VenteModel> iteratorVentes = postModel.getVentes().iterator();
        String postid = Utils.nextSessionId();
        StringBuilder req = new StringBuilder(
                "INSERT INTO posts(author, category, authorid, datepost, posttitre, more, etat, posttexte, postid)" +
                        "VALUES('Modelic','"+postModel.getCategory()+"','"+1+
                        "','"+dateF+"','"+postModel.getTitre()+"','','0','"+
                        postModel.getTexte()+"','"+ postid +"')");

        Logger.debug("Adding Post " + req.toString());
        int res;
        try {
            statement = c.createStatement();
            res = statement.executeUpdate(req.toString(), Statement.RETURN_GENERATED_KEYS);
            if (res == 0) {
                Logger.error("Post done");
                return false;
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
        }
        while (iterator.hasNext()) {
            try {
                doPostPhoto(iterator.next(), postid);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        while (iteratorVentes.hasNext()) {
            try {
                VenteModel venteModel = iteratorVentes.next();
                doPostVentes(venteModel.getUrl(), postid, venteModel.getTitre());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public boolean doPostPhoto(String url, String postid) throws SQLException {
        Connection c = DB.getConnection();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder req = new StringBuilder(
                "INSERT INTO photos(url, postid)" +
                        "VALUES(?,?)");
        PreparedStatement statement = c.prepareStatement(req.toString());
        statement.setObject(1,url, Types.VARCHAR);
        statement.setObject(2,postid,Types.VARCHAR);
        statement.executeUpdate();
        Db.closeQuietly(c);
        return true;
    }

    public boolean doPostVentes(String url, String postid, String titre) throws SQLException {
        Connection c = DB.getConnection();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        StringBuilder req = new StringBuilder(
                "INSERT INTO ventes(url, postid, titre)" +
                        "VALUES(?,?,?)");
        PreparedStatement statement = c.prepareStatement(req.toString());
        statement.setObject(1,url, Types.VARCHAR);
        statement.setObject(2,postid,Types.VARCHAR);
        statement.setObject(3,titre,Types.VARCHAR);
        statement.executeUpdate();
        Db.closeQuietly(c);
        return true;
    }

    @Override
    public boolean deletePost(String id) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "DELETE FROM POSTS" +
                        " WHERE id='"+id+"'");

        Logger.debug("Deleting Post " + req.toString());
        int res;
        try {
            statement = c.createStatement();
            res = statement.executeUpdate(req.toString());
            if (res == 0) {
                Logger.error("Delete Post done");
                return false;
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
        }
        return true;
    }

    @Override
    public boolean updatePost(String id, PostModel postModel) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "UPDATE posts SET author ='" +postModel.getAuthor()+"', " +
                        "category ='" +postModel.getCategory()+"', posttitre ='" +postModel.getTitre()+"', " +
                        "posttexte ='" +postModel.getTexte()+
                        "' WHERE postid='"+id+"'");

        Logger.debug("Updating Post " + req.toString());
        int res;
        try {
            statement = c.createStatement();
            res = statement.executeUpdate(req.toString());
            if (res == 0) {
                Logger.error("Update Post done");
                return false;
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
        }
        return true;
    }

    @Override
    public ArrayList<PostModel> getAllPosts(int page, int per_page, boolean all) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder(
                "SELECT * FROM posts ");
        req.append("ORDER BY id DESC");
        if (!all){
            if (page == 1) {
                req.append(" LIMIT ").append(per_page);
            } else if (page > -1) {
                req.append(" LIMIT ").append(per_page).append(" OFFSET ")
                        .append((page - 1) * per_page);
            } else {
                req.append(" LIMIT ").append(per_page);
            }
        }
        ArrayList<PostModel> models = new ArrayList<>();
        Logger.debug("getAllPosts " + req.toString());
        ResultSet res;
        try {
            statement = c.createStatement();
            res = statement.executeQuery(req.toString());
            models = extractModel(res);
            return models;
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return models;
        } finally {
            Db.closeQuietly(c);
        }
    }

    @Override
    public boolean validatePost(String id, String etat) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = null;
        if (etat.equals("0")){
            req = new StringBuilder(
                    "UPDATE posts SET etat ='1'"+
                            " WHERE id="+id+"");
        }else if(etat.equals("1")){
            req = new StringBuilder(
                    "UPDATE posts SET etat ='0'"+
                            " WHERE id="+id+"");
        }


        Logger.debug("Updating Post " + req.toString());
        int res;
        try {
            statement = c.createStatement();
            res = statement.executeUpdate(req.toString());
            if (res == 0) {
                Logger.error("Update Post done");
                return false;
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
            return false;
        } finally {
            Db.closeQuietly(c);
        }
        return true;
    }

    public ArrayList<String> getPostPhotos(String postid) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder("SELECT * FROM photos where postid='"
                + postid + "'");

        ArrayList<String> listPhoto = null;
        ResultSet res = null;
        try {
            statement = c.createStatement();
            res = statement.executeQuery(req.toString());
            listPhoto = extractPhoto(res);
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        } finally {
            Db.closeQuietly(c);
        }
        return listPhoto;
    }

    public ArrayList<VenteModel> getPostVentes(String postid) {
        Connection c = DB.getConnection();
        Statement statement;
        StringBuilder req = new StringBuilder("SELECT * FROM ventes where postid='"
                + postid + "'");

        ArrayList<VenteModel> listVentes = null;
        ResultSet res = null;
        try {
            statement = c.createStatement();
            res = statement.executeQuery(req.toString());
            listVentes = extractVente(res);
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        } finally {
            Db.closeQuietly(c);
        }
        return listVentes;
    }

    private static ArrayList<String> extractPhoto(ResultSet resultSet)
            throws SQLException {
        ArrayList<String> photos = new ArrayList<>();
        while (resultSet.next()) {
            String p = resultSet.getString("url");
            if (p != null) {
                photos.add(p);
            }
        }
        return photos;
    }

    private static ArrayList<VenteModel> extractVente(ResultSet resultSet)
            throws SQLException {
        ArrayList<VenteModel> ventes = new ArrayList<>();
        VenteModel venteModel = null;
        while (resultSet.next()) {
            String url = resultSet.getString("url");
            String titre = resultSet.getString("titre");
            if (url != null && titre != null) {
                venteModel = new VenteModel(titre, url);
                ventes.add(venteModel);
            }
        }
        return ventes;
    }

    private static ArrayList<PostModel> extractModel(ResultSet resultSet)
            throws SQLException {
        ArrayList<PostModel> opperationList = new ArrayList<>();
        while (resultSet.next()) {
            PostModel jrCptObject = getPostObject(resultSet);
            if (jrCptObject != null) {
                opperationList.add(jrCptObject);
            }
        }
        return opperationList;
    }

    private static PostModel getPostObject(ResultSet rs) {
        PostModel opp = new PostModel();

        try {
            opp.setId(rs.getString(ID) + "");
            opp.setAuthor(rs.getString(AUTHOR));
            opp.setPostid(rs.getString(POSTID));
            opp.setUrl(rs.getString(URL));
            opp.setAuthorid(rs.getString(AUTHORID));
            opp.setMore(rs.getString(MORE));
            opp.setPhotouser(rs.getString(PHOTOUSER));
            opp.setDate(rs.getString(DATEPOST));
            opp.setCategory(rs.getString(CATEGORY));
            opp.setSexe(rs.getString(SEXE));
            opp.setTelephone(rs.getString(TELEPHONE));
            opp.setPays(rs.getString(PAYS));
            opp.setPosttitre(rs.getString(POSTITRE));
            opp.setPosttexte(rs.getString(POSTTEXTE));
            opp.setEtat(rs.getString(ETAT));
            return opp;
        } catch (SQLException e) {
            Logger.error("Error getPostbject " + e.getMessage());

            return null;
        }
    }
}