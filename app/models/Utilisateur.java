package models;

/**
 * Created by Oumou on 06/05/2016.
 */
public class Utilisateur {
    private String idser;
    private String prenom;
    private String nom;
    private String etat;
    private String idprofil;
    private String login;
    private String password;
    private String isconnected;
    private String nbrtentative;
    private String email;
    private String telephone;
    private String datecreation;
    private String datemodification;
    private String datepwd;
    private String codeprofil;
    private String libelle;

    public Utilisateur() {
    }

    public Utilisateur(String idser, String nom, String prenom, String idprofil) {
        this.idser = idser;
        this.nom = nom;
        this.prenom = prenom;
        this.idprofil = idprofil;
    }

    public Utilisateur(String idprofil, String codeprofil, String libelle) {
        this.idprofil = idprofil;
        this.codeprofil = codeprofil;
        this.libelle = libelle;
    }

    public String getCodeprofil() {
        return codeprofil;
    }

    public void setCodeprofil(String codeprofil) {
        this.codeprofil = codeprofil;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getIdser() {
        return idser;
    }

    public void setIdser(String idser) {
        this.idser = idser;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getIdprofil() {
        return idprofil;
    }

    public void setIdprofil(String idprofil) {
        this.idprofil = idprofil;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIsconnected() {
        return isconnected;
    }

    public void setIsconnected(String isconnected) {
        this.isconnected = isconnected;
    }

    public String getNbrtentative() {
        return nbrtentative;
    }

    public void setNbrtentative(String nbrtentative) {
        this.nbrtentative = nbrtentative;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(String datecreation) {
        this.datecreation = datecreation;
    }

    public String getDatemodification() {
        return datemodification;
    }

    public void setDatemodification(String datemodification) {
        this.datemodification = datemodification;
    }

    public String getDatepwd() {
        return datepwd;
    }

    public void setDatepwd(String datepwd) {
        this.datepwd = datepwd;
    }


}
