package models;

/**
 * Created by ghambyte on 15/11/2017.
 */
public class VenteModel {
    String titre;
    String url;

    public VenteModel(String titre, String url) {
        this.titre = titre;
        this.url = url;
    }

    public VenteModel() {
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "VenteModel{" +
                "titre='" + titre + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
