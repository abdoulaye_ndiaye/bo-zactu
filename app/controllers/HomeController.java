package controllers;

import models.Utilisateur;
import play.Logger;
import play.Routes;
import play.mvc.*;

import tools.Const;
import views.html.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    public Result index() {
        return ok(index.render());
    }

    public Result accueil() {
        String session = session(Const.SESSION_CONNECTED);
        if (session == null) {
            Logger.info("unauthorized!");
            return ok(index.render());
        }
        Utilisateur users = new Utilisateur(session(Const.SESSION_ID_USER),session(Const.SESSION_NOM),
                session(Const.SESSION_PRENOM),session(Const.SESSION_PROFIL));
        return ok(accueil.render(Const.PROFIL_SUPERVISEUR, Const.PROFIL_UTILISATEUR,users));
    }

    public Result jsRoutes() {
        response().setContentType("text/javascript");
        return ok(Routes.javascriptRouter("appRoutes",
                routes.javascript.HomeController.index(),
                routes.javascript.UtilisateurController.connectUser(),
                routes.javascript.HomeController.accueil(),
                routes.javascript.PostController.getPosts(),
                routes.javascript.PostController.getAllPosts(),
                routes.javascript.PostController.deletePost(),
                routes.javascript.PostController.validatePost(),
                routes.javascript.PostController.updatePost(),
                routes.javascript.PostController.doPost(),
                routes.javascript.PostController.getPostPhotos(),
                routes.javascript.PostController.getPostVentes()

        ));
    }

}
