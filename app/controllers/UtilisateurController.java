package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import dao.DAOFactory;
import dao.UtilisateurDAO;
import dao.UtilisateurDaoImpl;
import models.Utilisateur;
import play.Logger;
import play.db.DB;
import play.libs.Json;
import play.mvc.Result;
import tools.Const;
import tools.Log;
import tools.Utils;
import views.html.index;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import static play.mvc.Controller.request;
import static play.mvc.Controller.session;
import static play.mvc.Results.ok;
import static play.mvc.Results.unauthorized;
import static tools.Utils.getObjectNode;
import static tools.Utils.getTotalRows;

/**
 * Created by Oumou on 06/05/2016.
 */
public class UtilisateurController {

    public Result getAllUsers(int page, int per_page) throws SQLException {
        UtilisateurDAO dao = DAOFactory.getUtilisateurDAO();
        ArrayList<Utilisateur> users = dao.getProfilUser();
        String totalRequest = "SELECT count(*) as total from utilisateur";
        int total = getTotalRows(totalRequest);
        int totalPage = total / per_page;
        if (total % per_page > 0) {
            totalPage++;
        }
        ObjectNode objectNode = Json.newObject();
        objectNode.put("result", "ok");
        objectNode.put("code", "1000");
        objectNode.put("message", "");
        objectNode.put("total", total);
        objectNode.put("total_page", totalPage);
        objectNode.put("current_page", page);
        objectNode.put("per_page", per_page);
        objectNode.put("users", Json.toJson(users));
        Logger.debug("utilisateur " + objectNode.toString());
        return ok(objectNode);
    }

    public Result connectUser() {
        Log.logActionHeader("nouvelle connexion d'un utilisateur");
        JsonNode json = request().body().asJson();
        ObjectNode user = Json.newObject();
        try {
            String login = json.findPath("login").textValue();
            String mdp = json.findPath("password").textValue();
            Logger.info("user Login " + login);
            Logger.info("user Password " + mdp);
            String[] data = new String[]{login, mdp};
            if (!Utils.checkData(data)) {
                return ok(Utils.getObjectNode("nok", "3001", "Parametres incorrects."));
            }
            UtilisateurDAO dao = DAOFactory.getUtilisateurDAO();
            ArrayList<Utilisateur> getUser = dao.connectUser(login, mdp);
            if(!getUser.isEmpty()) {
                if (!UtilisateurDaoImpl.profil.equals(Const.PROFIL_SUPERVISEUR) &&
                        !UtilisateurDaoImpl.profil.equals(Const.PROFIL_UTILISATEUR)) {
                    String message = "Parametres incorrects ou compte supprime/bloque.\n" +
                            "Veuillez verifier les valeurs saisies,\n"
                            + "ou contacter votre administrateur !!!";
                    user = Utils.getObjectNode("nok","3001",message);
                } else {
                    play.mvc.Controller.ctx().session().clear();
                    play.mvc.Controller.ctx().session().put(Const.SESSION_CONNECTED, "true");
                    play.mvc.Controller.ctx().session().put(Const.SESSION_LOGIN, login);
                    play.mvc.Controller.ctx().session().put(Const.SESSION_ID_USER, UtilisateurDaoImpl.iduser);
                    play.mvc.Controller.ctx().session().put(Const.SESSION_PROFIL, UtilisateurDaoImpl.profil);
                    play.mvc.Controller.ctx().session().put(Const.SESSION_NOM, UtilisateurDaoImpl.nom);
                    play.mvc.Controller.ctx().session().put(Const.SESSION_PRENOM, UtilisateurDaoImpl.prenom);
                    play.mvc.Controller.ctx().session().put(Const.SESSION_TEL, UtilisateurDaoImpl.tel);
                }
                user.put("result", "ok");
                user.put("code", "3000");
                user.put("message", "");
                user.put("users", Json.toJson(getUser));
                Logger.info("User connecte " + Json.toJson(getUser));
            }else {
                user.put("result", "nok");
                user.put("code", "3002");
                user.put("message", "Aucun user");
                user.put("users", Json.toJson(getUser));
                Logger.info("User connecte " + Json.toJson(getUser));
            }
            return ok(user);
        } catch (SQLException e) {
            Logger.error("getUser SQLException " + e.getMessage());
            ObjectNode result = Json.newObject();
            result = Utils.getObjectNode("nok","3005","Echec de la connexion!");
            return ok(result);
        }

    }

    public Result updatePwdUser(String mdp, String oldMdp) {
        Log.logActionHeader("updatePwdUser");

        String session = session(Const.SESSION_CONNECTED);
        if (session == null) {
            Logger.info("unauthorized!");
            return ok(index.render());
        }

        try {
            String idUser = session(Const.SESSION_ID_USER);

            if (mdp.equals(oldMdp)) {
                ObjectNode node = getObjectNode("nok", "",
                        "Vous devez choisir un mot de passe different de l'ancien.");
                Log.logActionOutput(node.toString());
                return ok(node);
            }

            try {
                UtilisateurDAO dao = DAOFactory.getUtilisateurDAO();
                boolean userUpdated = dao.updatePwdUser(idUser, mdp, oldMdp);

                Logger.info("REPONCE UPDATE USER " + userUpdated);

                if (!userUpdated) {
                    ObjectNode node = getObjectNode("nok", "3001",
                            "Parametres incorrect, Verifier les valeurs saisies !");
                    Log.logActionOutput(node.toString());
                    return ok(node);

                }
            } catch (Exception e) {
                Logger.error(e.getMessage());
            }
            ObjectNode node = getObjectNode("ok", "200",
                    "Votre mot de passe est modifie avec success!");
            Log.logActionOutput(node.toString());
            return ok(node);

        } catch (NullPointerException e) {
            Logger.error(e.getMessage());
            ObjectNode node = getObjectNode("nok", "3001", "Parametres incorrecte.");
            Log.logActionOutput(node.toString());
            return ok(node);
        }
    }

    public Result addUtilisateur(String prenom, String nom, String login, String email,
                                 String tel, String profil) throws SQLException {
        Log.logActionHeader("addUser");
        Connection connection = DB.getConnection();
        Statement stm = null;
        String session = session(Const.SESSION_CONNECTED);
        if (session == null) {
            ObjectNode node = getObjectNode("nosession", "3001", "Session Error");
            Log.logActionOutput(node.toString());
            return unauthorized(node);
        }
        try {

            stm = connection.createStatement();

            String request = "SELECT * FROM utilisateur WHERE LOGIN='" + login + "'";
            ResultSet resultSet = stm.executeQuery(request);

            if (resultSet.next()) {
                String message = "Erreur, Le login de l'utilisateur existe deja.\n" +
                        "Veuillez saisir SVP un login different.";
                ObjectNode node = getObjectNode("nok", "3001", message);
                Log.logActionOutput(node.toString());
                return ok(node);
            }
            //String message="VOTRE ABONNEMENT AU SERVICE SOGEPAY A BIEN ETE ENREGISTRE. VOTRE CODE SECRET EST "+generatedPass +". MERCI DE LE CHANGER AVANT LA PREMIERE UTILISATION.";

            UtilisateurDAO dao = DAOFactory.getUtilisateurDAO();
            String generatedPass = Utils.generateUserPassword();
            boolean newUser = dao.addUtilisateur(prenom, nom, login, email, tel, profil, generatedPass);

            Logger.info("REPONCE CREATION USER " + newUser);

            if (!newUser) {
                String message = "Parametres incorrect,La creation de lutilisateur a echoue !";
                ObjectNode node = getObjectNode("nok", "3001", message);
                Log.logActionOutput(node.toString());
                return ok(node);

            }
            ObjectNode node = getObjectNode("ok", "200", "Utilisateur cree avec success!\n" +
                    "Son nouveau mot de passe est : " + generatedPass);
            Log.logActionOutput(node.toString());
            return ok(node);

        } catch (NullPointerException e) {
            Logger.error(e.getMessage());
            ObjectNode node = getObjectNode("nok", "3001", "Erreur interne Parametres incorrecte.");
            Log.logActionOutput(node.toString());
            return ok(node);
        } finally {
            connection.close();
        }
    }

    public Result getProfilUser() {
        ObjectNode objectNode = Json.newObject();
        String session = session(Const.SESSION_CONNECTED);
        if (session == null) {
            Logger.info("unauthorized!");
            return ok(index.render());
        }
        try {
            ArrayList<Utilisateur> profil = new ArrayList<>();
            UtilisateurDAO dao = DAOFactory.getUtilisateurDAO();
            ArrayList<Utilisateur> users = dao.getProfilUser();
            profil= dao.getProfilUser();

            objectNode.put("result", "ok");
            objectNode.put("code", "1000");
            objectNode.put("message", "");
            objectNode.put("profilsUser", Json.toJson(profil));
            return ok(objectNode);

        } catch (SQLException e) {
            Logger.error(e.getMessage());
            objectNode.put("result", "nok");
            objectNode.put("code", "3004");
            objectNode.put("message", "Une erreur s'est produite.");
            return ok(objectNode);
        }
    }
}
