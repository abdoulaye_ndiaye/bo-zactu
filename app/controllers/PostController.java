package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import dao.DAOFactory;
import dao.PostDAO;
import io.swagger.annotations.*;
import models.PostModel;
import models.VenteModel;
import play.Logger;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import tools.Log;
import tools.Utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static tools.Const.DEFAULT_PER_PAGE;
import static tools.Utils.getObjectNode;
import static tools.Utils.getTotalRows;

/**
 * Created by mac on 19/09/2016.
 */
@Api(value = "/lesposts", description = "Renvoie les posts")
public class PostController extends Controller {
    @ApiOperation(
            nickname = "/lesposts",
            value = "List post",
            httpMethod = "GET",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Posts recuperes.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "page", value = "page courante", required = false, dataType = "int", paramType = "query")

    })
    public F.Promise<Result> getPosts(String authorId, String category, String sexe, int page, int per_page) {

        return F.Promise.promise(() -> {
            Logger.debug("THE PAGE ### " + page);
            PostDAO dao = DAOFactory.getPostDAO();
            ObjectNode result = Json.newObject();
            ArrayList<PostModel> list = dao.getListPosts(authorId, category, sexe, page, DEFAULT_PER_PAGE);

            if (list == null) {
                ObjectNode node = Utils.getObjectNode(451, "Aucun post trouve");
                Log.logActionOutput(node.toString());
                return status(451, node);
            }
            String totalRequest = "SELECT count(*) as total from posts";
            int total = getTotalRows(totalRequest);
            int totalPage = total / per_page;
            if (total % per_page > 0) {
                totalPage++;
            }
            result.put("total", total);
            result.put("total_page", totalPage);
            result.put("current_page", page);
            result.put("per_page", per_page);
            result.put("result", "ok");
            result.put("code", 200);
            result.set("posts", Json.toJson(list));
            Log.logActionOutput(result.toString());
            return ok(result);

        });
    }

    @ApiOperation(
            nickname = "/getPostById",
            value = "Détail du post",
            httpMethod = "GET",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post recupere.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    public F.Promise<play.mvc.Result> getPostById(String id) {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            ObjectNode result = Json.newObject();
            ArrayList<PostModel> list = dao.getPost(id);

            if (list == null) {
                ObjectNode node = Utils.getObjectNode(451, "Aucun post trouve");
                Log.logActionOutput(node.toString());
                return status(451, node);
            }

            result.put("code", 200);
            result.set("posts", Json.toJson(list));
            Log.logActionOutput(result.toString());
            return ok(result);

        });
    }

    @ApiOperation(
            nickname = "/dopost",
            value = "Faire un post",
            httpMethod = "POST",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post effectue.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(value = "Parametres du post", required = true, dataType = "models.PostModel", paramType = "body")
    })
    public F.Promise<play.mvc.Result> doPost() {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            Logger.info("JSON REQ: " + request().body());
            JsonNode json = request().body().asJson();
            try {
                String url0 = json.findPath("url0").textValue();
                String url1 = json.findPath("url1").textValue();
                String url2 = json.findPath("url2").textValue();
                String url3 = json.findPath("url3").textValue();
                String url4 = json.findPath("url4").textValue();
                String vente0 = json.findPath("vente0").textValue();
                String titre0 = json.findPath("titre0").textValue();
                String vente1 = json.findPath("vente1").textValue();
                String titre1 = json.findPath("titre1").textValue();
                String vente2 = json.findPath("vente2").textValue();
                String titre2 = json.findPath("titre2").textValue();
                String vente3 = json.findPath("vente3").textValue();
                String titre3 = json.findPath("titre3").textValue();
                String titre = json.findPath("titre").textValue();
                String texte = json.findPath("texte").textValue();
                String category = json.findPath("category").textValue();
                String[] data = new String[]{titre, texte, category};
                if (!Utils.checkData(data)) {
                    return ok(Utils.getObjectNode(450, "Parametres incorrects."));
                }
                List<String> photos = new ArrayList<String>();
                List<VenteModel> ventes = new ArrayList<VenteModel>();
                if (url0 != null && !url0.isEmpty()){
                    photos.add(url0);
                }
                if (url1 != null && !url1.isEmpty()){
                    photos.add(url1);
                }
                if (url2 != null && !url2.isEmpty()){
                    photos.add(url2);
                }
                if (url3 != null && !url3.isEmpty()){
                    photos.add(url3);
                }
                if (url4 != null && !url4.isEmpty()){
                    photos.add(url4);
                }
                if (vente0 != null && !vente0.isEmpty()){
                    VenteModel venteModel0 = new VenteModel(titre0, vente0);
                    ventes.add(venteModel0);
                }
                if (vente1 != null && !vente1.isEmpty()){
                    VenteModel venteModel1 = new VenteModel(titre1, vente1);
                    ventes.add(venteModel1);
                }
                if (vente2 != null && !vente2.isEmpty()){
                    VenteModel venteModel2 = new VenteModel(titre2, vente2);
                    ventes.add(venteModel2);
                }
                if (vente3 != null && !vente3.isEmpty()){
                    VenteModel venteModel3 = new VenteModel(titre3, vente3);
                    ventes.add(venteModel3);
                }
                PostModel postModel = new PostModel(category, texte, titre, photos, ventes);
                boolean postModels = false;
                try {
                    postModels = dao.doPost(postModel);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (!postModels) {
                    ObjectNode node = getObjectNode(452, "Aucun post");
                    Log.logActionOutput(node.toString());
                    return status(452, node);
                }

                ObjectNode node = getObjectNode(200, "Post bien enregistre.");
                Log.logActionOutput(node.toString());
                return ok(node);

            } catch (NullPointerException e) {
                Logger.error(e.getMessage());
                return ok(Utils.getObjectNode(450, "Parametres incorrects."));
            }

        });
    }

    @ApiOperation(
            nickname = "/delepost",
            value = "Supprimer un post",
            httpMethod = "POST",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post supprime.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(value = "Parametres du post", required = true, dataType = "String", paramType = "body")
    })
    public F.Promise<play.mvc.Result> deletePost() {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            Logger.info("JSON REQ: " + request().body());
            JsonNode json = request().body().asJson();
            String id = json.findPath("id").textValue();
            try {
                String[] data = new String[]{id};
                if (!Utils.checkData(data)) {
                    return ok(Utils.getObjectNode(450, "Parametres incorrects."));
                }
                boolean deletedPost = dao.deletePost(id);
                if (!deletedPost) {
                    ObjectNode node = getObjectNode(452, "Aucun post");
                    Log.logActionOutput(node.toString());
                    return status(452, node);
                }

                ObjectNode node = getObjectNode(200, "Post Supprime.");
                Log.logActionOutput(node.toString());
                return ok(node);

            } catch (NullPointerException e) {
                Logger.error(e.getMessage());
                return ok(Utils.getObjectNode(450, "Parametres incorrects."));
            }

        });
    }

    @ApiOperation(
            nickname = "/updatepost",
            value = "Mettre A jour un post",
            httpMethod = "POST",
            consumes = ("application/json"),
            produces = ("application/json"),
            response = PostModel.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Post mis A jour.", response = PostModel.class),
            @ApiResponse(code = 450, message = "Parametres incorrects.", response = PostModel.class),
            @ApiResponse(code = 451, message = "Donnees erronees.", response = PostModel.class),
    })
    @ApiImplicitParams(value = {
            @ApiImplicitParam(value = "Parametres du post", required = true, dataType = "models.PostModel", paramType = "body")
    })
    public F.Promise<play.mvc.Result> updatePost() {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            Logger.info("JSON REQ: " + request().body());
            JsonNode json = request().body().asJson();
            try {
                String id = json.findPath("id").textValue();
                String author = json.findPath("author").textValue();
                String category = json.findPath("category").textValue();
                String titre = json.findPath("posttitre").textValue();
                String texte = json.findPath("posttexte").textValue();
                String[] data = new String[]{id};
                if (!Utils.checkData(data)) {
                    return ok(Utils.getObjectNode(450, "Parametres incorrects."));
                }
                PostModel postModel = new PostModel(author, category, titre, texte);
                boolean deletedPost = dao.updatePost(id, postModel);
                if (!deletedPost) {
                    ObjectNode node = getObjectNode(452, "Aucun post");
                    Log.logActionOutput(node.toString());
                    return status(452, node);
                }

                ObjectNode node = getObjectNode(200, "Post Mis à jour.");
                node.put("result", "ok");
                Log.logActionOutput(node.toString());
                return ok(node);

            } catch (NullPointerException e) {
                Logger.error(e.getMessage());
                return ok(Utils.getObjectNode(450, "Parametres incorrects."));
            }

        });
    }

    public F.Promise<Result> getAllPosts(int page, int per_page) {

        return F.Promise.promise(() -> {
            Logger.debug("THE PAGE ### " + page);
            PostDAO dao = DAOFactory.getPostDAO();
            ObjectNode result = Json.newObject();
            ArrayList<PostModel> list = dao.getAllPosts(page, DEFAULT_PER_PAGE, false);

            if (list == null) {
                ObjectNode node = Utils.getObjectNode(451, "Aucun post trouve");
                Log.logActionOutput(node.toString());
                return status(451, node);
            }
            String totalRequest = "SELECT count(*) as total from posts";
            int total = getTotalRows(totalRequest);
            int totalPage = total / per_page;
            if (total % per_page > 0) {
                totalPage++;
            }
            result.put("total", total);
            result.put("total_page", totalPage);
            result.put("current_page", page);
            result.put("per_page", per_page);
            result.put("result", "ok");
            result.put("code", 200);
            result.set("posts", Json.toJson(list));
            Log.logActionOutput(result.toString());
            return ok(result);

        });
    }

    public F.Promise<Result> getPostPhotos(String idpost) {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            ObjectNode result = Json.newObject();
            ArrayList<String> list = dao.getPostPhotos(idpost);

            if (list == null) {
                ObjectNode node = Utils.getObjectNode(451, "Aucune photo trouvee");
                Log.logActionOutput(node.toString());
                return status(451, node);
            }
            result.put("result", "ok");
            result.put("code", 200);
            result.set("photos", Json.toJson(list));
            Log.logActionOutput(result.toString());
            return ok(result);
        });
    }

    public F.Promise<Result> getPostVentes(String idpost) {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            ObjectNode result = Json.newObject();
            ArrayList<VenteModel> listVentes = dao.getPostVentes(idpost);

            if (listVentes == null) {
                ObjectNode node = Utils.getObjectNode(451, "Aucune vente trouvee");
                Log.logActionOutput(node.toString());
                return status(451, node);
            }
            result.put("result", "ok");
            result.put("code", 200);
            result.set("ventes", Json.toJson(listVentes));
            Log.logActionOutput(result.toString());
            return ok(result);
        });
    }

    public F.Promise<play.mvc.Result> validatePost() {

        return F.Promise.promise(() -> {
            PostDAO dao = DAOFactory.getPostDAO();
            Logger.info("JSON REQ: " + request().body());
            JsonNode json = request().body().asJson();
            String id = json.findPath("id").textValue();
            String etat = json.findPath("etat").textValue();
            try {
                String[] data = new String[]{id};
                if (!Utils.checkData(data)) {
                    return ok(Utils.getObjectNode(450, "Parametres incorrects."));
                }
                boolean deletedPost = dao.validatePost(id, etat);
                if (!deletedPost) {
                    ObjectNode node = getObjectNode(452, "Aucun post");
                    Log.logActionOutput(node.toString());
                    return status(452, node);
                }

                ObjectNode node = getObjectNode(200, "Post Validé.");
                Log.logActionOutput(node.toString());
                return ok(node);

            } catch (NullPointerException e) {
                Logger.error(e.getMessage());
                return ok(Utils.getObjectNode(450, "Parametres incorrects."));
            }

        });
    }

}
